package org.scrg.sample;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class ApplicationTests {

	@Autowired
	private WebApplicationContext wac;
	
	private MockMvc mockMvc;
	
	@Before
	public void setup() {
		mockMvc = webAppContextSetup(wac).build();
	}

	/*
	@Test
	public void search() throws Exception {
		mockMvc.perform(get("/Search"))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(view().name("hoge"));
	
	}
	*/
	
	@Test
	public void contextLoads() {
	}

	/*
	@Test
	public void home() throws Exception {
		mockMvc.perform(get("/"))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(view().name("hoge"));
	
	}
	*/
	
	/*
	@Test
	public void fileAddのテスト() throws Exception {
		byte[] fileImage=null;
		Path path = Paths.get("src/test/resources/test.jpg");
		if (Files.exists(path, LinkOption.NOFOLLOW_LINKS)) {
			fileImage = Files.readAllBytes(path);
		}
		String fileName = "ajaxText.jpg";
		MockMultipartFile file = new MockMultipartFile("uploadFile" , fileName,
				null,fileImage);
		mockMvc.perform(fileUpload("/ajax/fileAdd").file(file))
			.andDo(print())
			.andExpect(status().isOk());
	}
	*/
}
