$(function() {

//	alert("ready!");
	
	setCsrfTokenToAjaxHeader();

	$("#ajaxTest").click(
			function(e) {
				e.preventDefault(); // prevent actual form submit and page reload
//				alert("ajaxTest clicked!");
				var requestUrl = "ajax/test"
//				alert("url: " + requestUrl);

					var userName = $("#name").val();
				var userAge = $("#age").val();
//				alert("userName: " + userName);
//				alert("userAge: " + userAge);

				var jsonData = {
						'name' : userName,
						'age' : userAge
				}
//				alert("jsonData: " + jsonData);
				alert("jsonData: " + JSON.stringify(jsonData));
				$.ajax({
					url:requestUrl,
					type: 'post',
					contentType: "application/json;charset=UTF-8",
					data: JSON.stringify(jsonData),
					dataType: 'text',
					complete: function(){},
					success: function(data) {
						alert("ajax/test success! " + data);
						$("#response").html(data);
					} ,
					error: function(XMLHttpRequest,
							textStatus, errorThrown) {
						alert("database ajax通信エラー" + XMLHttpRequest.status + " : " +
								textStatus + " : " + errorThrown );
					}
				})
			}
	)

	$("#testFile").change(
			function(e) {
				e.preventDefault(); // prevent actual form submit and page reload
//				alert("ajaxTest clicked!");
				var requestUrl = "ajax/testFile"
				alert("url: " + requestUrl);
				
				file = $(this).prop('files')[0];

				alert("file name " + file.name);

				//画像の場合
				if ( file.type.match('image.*')){

					//サムネイル画像表示
					var reader = new FileReader();
					reader.onload = function() {
						var img_src = $('<img>').attr({'src': reader.result , 'width':'200'});
						$('#imageShow').html(img_src);
						$('#imageShow').show();
					};
					reader.readAsDataURL(file);

				}
				//画像以外のファイルの場合
				else {
//					$('span').html("");
					$('#imageShow').hide();

				}
				alert("imageShow");
				
				//サーバーへアップロード exif情報を受信する
				var formData = new FormData();
				formData.append('file',file);
				alert("file: " + file);

				var requrl = 'ajax/fileTest';
				alert("requrl: " + requrl);

				$.ajax({
					url:requrl,
					type: 'post',
					data: formData,
					processData: false,
					contentType: false,
					dataType: 'html',
					complete:function(){},
					success: function(res) {
						alert("res: " + res);
					},
					error : function(XMLHttpRequest,
							textStatus, errorThrown) {
						alert("ajax通信エラー: " + XMLHttpRequest.status + " : " + textStatus + " : "
								+ errorThrown );
					}

				});

			}
	)

});

function setCsrfTokenToAjaxHeader() {
	
//	alert("setCsrfToken");

	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
}
