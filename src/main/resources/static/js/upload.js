// ファイルのアップロードに関するJavaScript
//

$(function() {
//	alert("upload: " );

//	alert("ready!");

	setCsrfTokenToAjaxHeader();
	
	$("#register").attr('disabled', true);//ファイルを選択するまでは登録ボタンを無効にする
	
	//validate 各エレメントにrequired,messagesの設定
	$("#fileDetail").validate({
		rules : {
			ocr_time: {
				required: [true , '発生日時']
			},
			p_longitude: {
				required: [true , '経度']
			},
			p_latitude: {
				required: [true , '緯度']
			},
			sns_id: {
				required: [true , 'センサID']
			},
			sns_tp: {
				required: [true , 'センサ種別'],
				digits: true
			},
			sns_op_st: {
				required: [true , 'センサ動作状態'],
				digits: true
			},
			sns_dt_st_tp: {
				required: [true , 'センサデータ状態種別'],
				digits: true
			},
			adm_id: {
				required: [true , '管理者ID']
			},
			acc_hst_id: {
				required: [true , 'アクセス履歴ID']
			},
			acc_aut_tp: {
				required: [true , 'アクセス権限種別'],
				digits: true
			},
			rg_time: {
				required: [true , '新規登録日時']
			},
			up_time: {
				required: [true , '更新日時']
			}

		},

		message: {
			required: $.format('{1}は必須入力項目です。')
		},

		errorClass: "myError",
		errorLabelContainer: "#errorList",
		wrapper: "li"
	});

	// -----------------------------------------------------------------
	//
	//アップロードするファイルを選択した時
	//

	$("#file").change(function(e) {
			$("#register").attr('disabled',false);//登録ボタンを有効にする
			e.preventDefault(); // prevent actual form submit and page reload
			file = $(this).prop('files')[0];
//			alert("file.type= " + file.type + "***" );
			if ( file.type == "" ) {
//				alert("file.type=null: " + file.type );
				$('#snsrwdtcontent').val("other/other");
				$('#sns_rw_dt_content').val("other/other");
			}
			else {
				$('#snsrwdtcontent').val(file.type);
				$('#sns_rw_dt_content').val(file.type);
			}

//			alert("file name " + file.name);

			//画像の場合
			if ( file.type.match('image.*')){

				//サムネイル画像表示
				var reader = new FileReader();
				reader.onload = function() {
					var img_src = $('<img>').attr({'src': reader.result , 'width':'200'});
					$('#imageShow').html(img_src);
					$('#imageShow').show();
				};
				reader.readAsDataURL(file);

			}
			//画像以外のファイルの場合
			else {
//				$('span').html("");
				$('#imageShow').hide();

			}
//			alert("imageShow");

			//サーバーへアップロード exif情報を受信する
			var formData = new FormData();
			formData.append('file',file);
//			alert("file: " + file);

			var requrl = 'ajax/fileScan';
//			alert("requrl: " + requrl);

			$.ajax({
				url:requrl,
				type: 'post',
				data: formData,
				processData: false,
				contentType: false,
//				contentType: "application/json;charset=UTF-8",
//				enctype  : 'multipart/form-data',
				dataType: 'json',
//				success: onSuccess,
//				error: onError
				complete:function(){},
				success: function(res) {
//					alert("res: " + JSON.stringify(res));
					$('#recid').val(res.rec_id);
					$('#rec_id').val(res.rec_id);
					$('#ocrtime').val(res.ocr_time);
					$('#ocr_time').val(res.ocr_time);
					$('#longitude').val(res.p_longitude);
					$('#p_longitude').val(res.p_longitude);
					$('#latitude').val(res.p_latitude);
					$('#p_latitude').val(res.p_latitude);
					$('#snsid').val(res.sns_id);
					$('#sns_id').val(res.sns_id);
					$('#snstp').val(res.sns_tp);
					$('#sns_tp').val(res.sns_tp);
					$('#snsopst').val(res.sns_op_st);
					$('#sns_op_st').val(res.sns_op_st);
					$('#snsdtsttp').val(res.sns_dt_st_tp);
					$('#sns_dt_st_tp').val(res.sns_dt_st_tp);
					$('#admid').val(res.adm_id);
					$('#adm_id').val(res.adm_id);
					$('#acchstid').val(res.acc_hst_id);
					$('#acc_hst_id').val(res.acc_hst_id);
					$('#accauttp').val(res.acc_aut_tp);
					$('#acc_aut_tp').val(res.acc_aut_tp);
					$('#rgtime').val(res.rg_time);
					$('#rg_time').val(res.rg_time);
					$('#uptime').val(res.up_time);
					$('#up_time').val(res.up_time);
				},
				error : function(XMLHttpRequest,
						textStatus, errorThrown) {
					alert("ajax通信エラー: " + XMLHttpRequest.status + " : " + textStatus + " : "
							+ errorThrown );
				}

			});

	});

		// -----------------------------------------------------------------
		//
		// データのuploadボタンのクリック時
		//

	$("#upload").click(
			function(e) {
				e.preventDefault(); // prevent actual form submit and page reload
//				alert("upload clicked!");

				file = $("#file").prop('files')[0];
				if (file) {
//					alert( "file: " + file + " type: " + file.type + " name: " + file.name);
				}
				else {
					return;
				}

				//ファイル情報を送信する
				var formData = new FormData();
				formData.append('file',file);
//				alert( "formData: " + formData );

				var requrl = 'ajax/upload';
//				alert("url: " + requrl);

				$.ajax({
					url:requrl,
					type: 'post',
					data: formData,
					processData: false,
					contentType: false,
//					enctype  : 'multipart/form-data',
					dataType: 'text',
					complete:function(){},
					success: function(res) {
//						alert("res: " + res);
					    $('input[type="text"]').val("");
			    		$("#file").val("");
//					    $("#file").replaceWith('<input type="file" name="selectedFile" id="file" />');
					    $("#imageShow").hide();
					},
					error:function(XMLHttpRequest,
							textStatus, errorThrown) {
						alert("ajax通信エラー" + XMLHttpRequest.status);
					}
				});


	});

		// -----------------------------------------------------------------
		//
		// データの新規追加・登録
		//

	$("#register").click(
			function(e) {
//				alert('pushed register button');
				e.preventDefault(); // prevent actual form submit and page reload
				 $(this).find('#register').attr('disabled', 'disabled');
				 //validate
				$("#fileDetail").validate().form();

				if (!$("#fileDetail").valid()) {
					alert('not valid');
					return false;
				}

				//ファイル情報を送信する
				var form = $('#ajaxFile').get(0);
				var formData = new FormData(form);
//				alert("formData: " + formData);
/*
				var formData = new FormData();
				alert("prop" + $("#fileSelect").prop('files'));
				alert("file: " + file + "*");
				formData.append('file',file);
*/
				var requrl = 'ajax/fileAdd';
//				alert("url: " + requrl);

				$.ajax({
					url:requrl,
					type: 'post',
					data: formData,
					processData: false,
					contentType: false,
//					enctype  : 'multipart/form-data',
					dataType: 'text',
					complete:function(){},
					success: function(res) {
//						alert("success: " + res);
//						$("#XMLHttpRequestPost").html(res);
						$("#snsrwdtname").val(res);
						$("#sns_rw_dt_name").val(res);


						//引き続きフォーム内のデータ（ClientData）を送信する
						var requrlJson = 'ajax/dataAdd';
//						alert('form data url: ' + requrlJson);

						var recid = 0;
						var occurtime = $("#ocr_time").val();
						var longitude = +$("#p_longitude").val();
						var latitude = +$("#p_latitude").val();
						var sensorshubetsu = $("#sns_tp").val();
//						alert('sensor shubetsu: ' + sensorshubetsuPost);
						var sensorid = +$("#sns_id").val();
						var sensorjoutai = $("#sns_op_st").val();
//						alert('sensor joutai: ' + sensorjoutaiPost);
						var kannrishaid = +$("#adm_id").val();
						var accesskengen = $("#acc_aut_tp").val();
						var accessrireki = $("#acc_hst_id").val();
						var sensordata = $("#sns_dt_st_tp").val();
						var torokunichiji = $("#rg_time").val();
//						alert('toroku: ' + torokunichijiPut);
						var koshinnichiji = $("#up_time").val();
						var namadata = $("#sns_rw_dt_name").val();
//						alert('snsrwdtname' + namadata);
						var namacontent = $("#sns_rw_dt_content").val();
						var resultdata = "";
						var resultcontent = "";
//						var location = "LOCATION(" + longitudePost.toString() + " " + latitudePost.toString() + ")";
//						alert('location ' + location);

						var jsonData = {
							'rec_id' : recid,
							'ocr_time' : occurtime,
							'p_latitude' : latitude,
							'p_longitude' : longitude,
//							'location' : location,
							'sns_tp' : sensorshubetsu,
							'sns_id': sensorid,
							'sns_op_st' : sensorjoutai,
							'adm_id' : kannrishaid,
							'acc_aut_tp' : accesskengen,
							'acc_hst_id' : accessrireki,
							'sns_dt_st_tp' : sensordata,
							'sns_rw_dt_name' : namadata,
							'sns_dt_pr_rslt_content' : resultcontent,
							'sns_dt_pr_rslt_name' : resultdata,
							'sns_rw_dt_content' : namacontent,
							'rg_time' : torokunichiji,
							'up_time' : koshinnichiji
//							'rawFile' : namadata,
//							'resultFile' : resultdata
						};
//						alert('json String: ' + JSON.stringify(jsonData));
//						alert('json: ' + jsonData);

						$.ajax({
							url : requrlJson ,
							type : 'POST',
							contentType: "application/json;charset=UTF-8",
//							data : jsonData,
							data : JSON.stringify(jsonData),
//							contentType : "application/x-www-form-urlencoded; charset=UTF-8",
//							dataType : 'json',
							dataType : 'text',
							success : function(data, tetStatus, xhr) {
//								alert("database adding: " + JSON.stringify(data) );
								window.location = "";//同ページを再表示させる

								if ( data == null || data == "" ){
									window.location = "";//同ページを再表示させる
								}
								else {
									$("#errorMessageList").css("color","#c85554");
//									$("#errorList").css("display","inline");
									$.each( data , function() {
//										alert("data each: " + this );
										$("#errorList").append("<li><label style=\"display: inline;\" class=\"myError\">" + this + "</label></li>");
//										$("#errorMessageList").append("<li>" + this + "</li>");
									});
									return;
								}
								$('#form')[1].reset();
								$('#form')[2].reset();
							},
							error : function(XMLHttpRequest,
									textStatus, errorThrown) {
								alert("database ajax通信エラー" + XMLHttpRequest.status + " : " +
										textStatus + " : " + errorThrown );
							}
						});


					},
					error : function(XMLHttpRequest,
							textStatus, errorThrown) {
						alert("file storage ajax通信エラー" + XMLHttpRequest.status + " : " +
								textStatus + " : " + errorThrown );
					}

				});
	});
});

function setCsrfTokenToAjaxHeader() {
//	alert("setCsrfTokenToAjaxHeader()");
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
}

function onSuccess(data,status) {
	alert(data);
}

function onError(data, status) {
	alert("err");
}