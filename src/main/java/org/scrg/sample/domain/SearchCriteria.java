package org.scrg.sample.domain;

public class SearchCriteria {
	
	private String accessCR;
	private String adminCR;
	private String sensorCR;
	private String latitude1;
	private String longitude1;
	private String latitude2;
	private String longitude2;
	private String radius;
	private String offSet;
	private String limit;
	private String how;
	private String contentType;
	private Boolean byDate;
	public String getAccessCR() {
		return accessCR;
	}
	public void setAccessCR(String accessCR) {
		this.accessCR = accessCR;
	}
	public String getAdminCR() {
		return adminCR;
	}
	public void setAdminCR(String adminCR) {
		this.adminCR = adminCR;
	}
	public String getSensorCR() {
		return sensorCR;
	}
	public void setSensorCR(String sensorCR) {
		this.sensorCR = sensorCR;
	}
	public String getLatitude1() {
		return latitude1;
	}
	public void setLatitude1(String latitude1) {
		this.latitude1 = latitude1;
	}
	public String getLongitude1() {
		return longitude1;
	}
	public void setLongitude1(String longitude1) {
		this.longitude1 = longitude1;
	}
	public String getLatitude2() {
		return latitude2;
	}
	public void setLatitude2(String latitude2) {
		this.latitude2 = latitude2;
	}
	public String getLongitude2() {
		return longitude2;
	}
	public void setLongitude2(String longitude2) {
		this.longitude2 = longitude2;
	}
	public String getRadius() {
		return radius;
	}
	public void setRadius(String radius) {
		this.radius = radius;
	}
	public String getOffSet() {
		return offSet;
	}
	public void setOffSet(String offSet) {
		this.offSet = offSet;
	}
	public String getLimit() {
		return limit;
	}
	public void setLimit(String limit) {
		this.limit = limit;
	}
	public String getHow() {
		return how;
	}
	public void setHow(String how) {
		this.how = how;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public Boolean getByDate() {
		return byDate;
	}
	public void setByDate(Boolean byDate) {
		this.byDate = byDate;
	}
	
	public SearchCriteria(String accessCR, String adminCR, String sensorCR,
			String latitude1, String longitude1, String latitude2,
			String longitude2, String radius, String offSet, String limit,
			String how, String contentType, Boolean byDate) {
		this.accessCR = accessCR;
		this.adminCR = adminCR;
		this.sensorCR = sensorCR;
		this.latitude1 = latitude1;
		this.longitude1 = longitude1;
		this.latitude2 = latitude2;
		this.longitude2 = longitude2;
		this.radius = radius;
		this.offSet = offSet;
		this.limit = limit;
		this.how = how;
		this.contentType = contentType;
		this.byDate = byDate;
	}

	public SearchCriteria() {}

	@Override
	public String toString() {
		return "SearchCriteria [accessCR=" + accessCR + ", adminCR=" + adminCR
				+ ", sensorCR=" + sensorCR + ", latitude1=" + latitude1
				+ ", longitude1=" + longitude1 + ", latitude2=" + latitude2
				+ ", longitude2=" + longitude2 + ", radius=" + radius
				+ ", offSet=" + offSet + ", limit=" + limit + ", how=" + how
				+ ", contentType=" + contentType + ", byDate=" + byDate + "]";
	}
	
}
