package org.scrg.sample.domain;

public class LocationData {
	
    private int rec_id;
    private double p_latitude;
    private double p_longitude;
    private String sns_rw_dt_content;

    public int getRec_id() {
		return rec_id;
	}
	public void setRec_id(int rec_id) {
		this.rec_id = rec_id;
	}
	public double getP_latitude() {
		return p_latitude;
	}
	public void setP_latitude(double p_latitude) {
		this.p_latitude = p_latitude;
	}
	public double getP_longitude() {
		return p_longitude;
	}
	public void setP_longitude(double p_longitude) {
		this.p_longitude = p_longitude;
	}
	
	public String getSns_rw_dt_content() {
		return sns_rw_dt_content;
	}
	public void setSns_rw_dt_content(String sns_rw_dt_content) {
		this.sns_rw_dt_content = sns_rw_dt_content;
	}
	
	public LocationData(int rec_id, double p_latitude, double p_longitude,
			String sns_rw_dt_content) {
		this.rec_id = rec_id;
		this.p_latitude = p_latitude;
		this.p_longitude = p_longitude;
		this.sns_rw_dt_content = sns_rw_dt_content;
	}

    public LocationData() { }

}
