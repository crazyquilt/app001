package org.scrg.sample.domain;

public class ShowData {
	
	private String rec_id;
	private String ocr_time;
	private String p_latitude;
	private String p_longitude;
	private String sns_tp;
	private String sns_id;
	private String sns_op_st;
	private String adm_id;
	private String acc_aut_tp;
	private String acc_hst_id;
	private String sns_dt_st_tp;
	private String sns_dt_pr_rslt_name;
	private String sns_dt_pr_rslt_content;
	private String sns_rw_dt_name;
	private String sns_rw_dt_content;
	private String rg_time;
	private String up_time;
	private String image_url;
	
	public String getRec_id() {
		return rec_id;
	}
	public void setRec_id(String rec_id) {
		this.rec_id = rec_id;
	}
	public String getOcr_time() {
		return ocr_time;
	}
	public void setOcr_time(String ocr_time) {
		this.ocr_time = ocr_time;
	}
	public String getP_latitude() {
		return p_latitude;
	}
	public void setP_latitude(String p_latitude) {
		this.p_latitude = p_latitude;
	}
	public String getP_longitude() {
		return p_longitude;
	}
	public void setP_longitude(String p_longitude) {
		this.p_longitude = p_longitude;
	}
	public String getSns_tp() {
		return sns_tp;
	}
	public void setSns_tp(String sns_tp) {
		this.sns_tp = sns_tp;
	}
	public String getSns_id() {
		return sns_id;
	}
	public void setSns_id(String sns_id) {
		this.sns_id = sns_id;
	}
	public String getSns_op_st() {
		return sns_op_st;
	}
	public void setSns_op_st(String sns_op_st) {
		this.sns_op_st = sns_op_st;
	}
	public String getAdm_id() {
		return adm_id;
	}
	public void setAdm_id(String adm_id) {
		this.adm_id = adm_id;
	}
	public String getAcc_aut_tp() {
		return acc_aut_tp;
	}
	public void setAcc_aut_tp(String acc_aut_tp) {
		this.acc_aut_tp = acc_aut_tp;
	}
	public String getAcc_hst_id() {
		return acc_hst_id;
	}
	public void setAcc_hst_id(String acc_hst_id) {
		this.acc_hst_id = acc_hst_id;
	}
	public String getSns_dt_st_tp() {
		return sns_dt_st_tp;
	}
	public void setSns_dt_st_tp(String sns_dt_st_tp) {
		this.sns_dt_st_tp = sns_dt_st_tp;
	}
	public String getSns_dt_pr_rslt_name() {
		return sns_dt_pr_rslt_name;
	}
	public void setSns_dt_pr_rslt_name(String sns_dt_pr_rslt_name) {
		this.sns_dt_pr_rslt_name = sns_dt_pr_rslt_name;
	}
	public String getSns_dt_pr_rslt_content() {
		return sns_dt_pr_rslt_content;
	}
	public void setSns_dt_pr_rslt_content(String sns_dt_pr_rslt_content) {
		this.sns_dt_pr_rslt_content = sns_dt_pr_rslt_content;
	}
	public String getSns_rw_dt_name() {
		return sns_rw_dt_name;
	}
	public void setSns_rw_dt_name(String sns_rw_dt_name) {
		this.sns_rw_dt_name = sns_rw_dt_name;
	}
	public String getSns_rw_dt_content() {
		return sns_rw_dt_content;
	}
	public void setSns_rw_dt_content(String sns_rw_dt_content) {
		this.sns_rw_dt_content = sns_rw_dt_content;
	}
	public String getRg_time() {
		return rg_time;
	}
	public void setRg_time(String rg_time) {
		this.rg_time = rg_time;
	}
	public String getUp_time() {
		return up_time;
	}
	public void setUp_time(String up_time) {
		this.up_time = up_time;
	}
		public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	
	public ShowData(String rec_id, String ocr_time, String p_latitude,
			String p_longitude, String sns_tp, String sns_id, String sns_op_st,
			String adm_id, String acc_aut_tp, String acc_hst_id,
			String sns_dt_st_tp, String sns_dt_pr_rslt_name,
			String sns_dt_pr_rslt_content, String sns_rw_dt_name,
			String sns_rw_dt_content, String rg_time, String up_time,
			String image_url) {
		this.rec_id = rec_id;
		this.ocr_time = ocr_time;
		this.p_latitude = p_latitude;
		this.p_longitude = p_longitude;
		this.sns_tp = sns_tp;
		this.sns_id = sns_id;
		this.sns_op_st = sns_op_st;
		this.adm_id = adm_id;
		this.acc_aut_tp = acc_aut_tp;
		this.acc_hst_id = acc_hst_id;
		this.sns_dt_st_tp = sns_dt_st_tp;
		this.sns_dt_pr_rslt_name = sns_dt_pr_rslt_name;
		this.sns_dt_pr_rslt_content = sns_dt_pr_rslt_content;
		this.sns_rw_dt_name = sns_rw_dt_name;
		this.sns_rw_dt_content = sns_rw_dt_content;
		this.rg_time = rg_time;
		this.up_time = up_time;
		this.image_url = image_url;
	}
	public ShowData() {}
	@Override
	public String toString() {
		return "ShowData [rec_id=" + rec_id + ", ocr_time=" + ocr_time
				+ ", p_latitude=" + p_latitude + ", p_longitude=" + p_longitude
				+ ", sns_tp=" + sns_tp + ", sns_id=" + sns_id + ", sns_op_st="
				+ sns_op_st + ", adm_id=" + adm_id + ", acc_aut_tp="
				+ acc_aut_tp + ", acc_hst_id=" + acc_hst_id + ", sns_dt_st_tp="
				+ sns_dt_st_tp + ", sns_dt_pr_rslt_name=" + sns_dt_pr_rslt_name
				+ ", sns_dt_pr_rslt_content=" + sns_dt_pr_rslt_content
				+ ", sns_rw_dt_name=" + sns_rw_dt_name + ", sns_rw_dt_content="
				+ sns_rw_dt_content + ", rg_time=" + rg_time + ", up_time="
				+ up_time + ", image_url=" + image_url + "]";
	}

}
