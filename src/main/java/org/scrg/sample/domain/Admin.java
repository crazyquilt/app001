package org.scrg.sample.domain;

public class Admin {
	
	private int admin_id;
	private String admin_name;
	public int getAdmin_id() {
		return admin_id;
	}
	public void setAdmin_id(int admin_id) {
		this.admin_id = admin_id;
	}
	public String getAdmin_name() {
		return admin_name;
	}
	public void setAdmin_name(String admin_name) {
		this.admin_name = admin_name;
	}
	public Admin(int admin_id, String admin_name) {
		this.admin_id = admin_id;
		this.admin_name = admin_name;
	}

	public Admin() {}
	@Override
	public String toString() {
		return "Admin [admin_id=" + admin_id + ", admin_name=" + admin_name
				+ "]";
	}

}
