package org.scrg.sample.domain;

import java.sql.Timestamp;

public class Data {
	
    private int rec_id;
    private Timestamp ocr_time;
    private String location;
    private double p_latitude;
    private double p_longitude;
    private int sns_tp;
    private int sns_id;
    private int sns_op_st;
    private int adm_id;
    private int acc_aut_tp;
    private int acc_hst_id;
    private int sns_dt_st_tp;
    private String sns_dt_pr_rslt_name;
    private String sns_dt_pr_rslt_content;
    private String sns_rw_dt_name;
    private String sns_rw_dt_content;
	private Timestamp rg_time;
    private Timestamp up_time;
	public int getRec_id() {
		return rec_id;
	}
	public void setRec_id(int rec_id) {
		this.rec_id = rec_id;
	}
	public Timestamp getOcr_time() {
		return ocr_time;
	}
	public void setOcr_time(Timestamp ocr_time) {
		this.ocr_time = ocr_time;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public double getP_latitude() {
		return p_latitude;
	}
	public void setP_latitude(double p_latitude) {
		this.p_latitude = p_latitude;
	}
	public double getP_longitude() {
		return p_longitude;
	}
	public void setP_longitude(double p_longitude) {
		this.p_longitude = p_longitude;
	}
	public int getSns_tp() {
		return sns_tp;
	}
	public void setSns_tp(int sns_tp) {
		this.sns_tp = sns_tp;
	}
	public int getSns_id() {
		return sns_id;
	}
	public void setSns_id(int sns_id) {
		this.sns_id = sns_id;
	}
	public int getSns_op_st() {
		return sns_op_st;
	}
	public void setSns_op_st(int sns_op_st) {
		this.sns_op_st = sns_op_st;
	}
	public int getAdm_id() {
		return adm_id;
	}
	public void setAdm_id(int adm_id) {
		this.adm_id = adm_id;
	}
	public int getAcc_aut_tp() {
		return acc_aut_tp;
	}
	public void setAcc_aut_tp(int acc_aut_tp) {
		this.acc_aut_tp = acc_aut_tp;
	}
	public int getAcc_hst_id() {
		return acc_hst_id;
	}
	public void setAcc_hst_id(int acc_hst_id) {
		this.acc_hst_id = acc_hst_id;
	}
	public int getSns_dt_st_tp() {
		return sns_dt_st_tp;
	}
	public void setSns_dt_st_tp(int sns_dt_st_tp) {
		this.sns_dt_st_tp = sns_dt_st_tp;
	}
	public String getSns_dt_pr_rslt_name() {
		return sns_dt_pr_rslt_name;
	}
	public void setSns_dt_pr_rslt_name(String sns_dt_pr_rslt_name) {
		this.sns_dt_pr_rslt_name = sns_dt_pr_rslt_name;
	}
	public String getSns_dt_pr_rslt_content() {
		return sns_dt_pr_rslt_content;
	}
	public void setSns_dt_pr_rslt_content(String sns_dt_pr_rslt_content) {
		this.sns_dt_pr_rslt_content = sns_dt_pr_rslt_content;
	}
	public String getSns_rw_dt_name() {
		return sns_rw_dt_name;
	}
	public void setSns_rw_dt_name(String sns_rw_dt_name) {
		this.sns_rw_dt_name = sns_rw_dt_name;
	}
	public String getSns_rw_dt_content() {
		return sns_rw_dt_content;
	}
	public void setSns_rw_dt_content(String sns_rw_dt_content) {
		this.sns_rw_dt_content = sns_rw_dt_content;
	}
	public Timestamp getRg_time() {
		return rg_time;
	}
	public void setRg_time(Timestamp rg_time) {
		this.rg_time = rg_time;
	}
	public Timestamp getUp_time() {
		return up_time;
	}
	public void setUp_time(Timestamp up_time) {
		this.up_time = up_time;
	}
	public Data(int rec_id, Timestamp ocr_time, String location,
			double p_latitude, double p_longitude, int sns_tp, int sns_id,
			int sns_op_st, int adm_id, int acc_aut_tp, int acc_hst_id,
			int sns_dt_st_tp, String sns_dt_pr_rslt_name,
			String sns_dt_pr_rslt_content, String sns_rw_dt_name,
			String sns_rw_dt_content, Timestamp rg_time, Timestamp up_time) {

		this.rec_id = rec_id;
		this.ocr_time = ocr_time;
		this.location = location;
		this.p_latitude = p_latitude;
		this.p_longitude = p_longitude;
		this.sns_tp = sns_tp;
		this.sns_id = sns_id;
		this.sns_op_st = sns_op_st;
		this.adm_id = adm_id;
		this.acc_aut_tp = acc_aut_tp;
		this.acc_hst_id = acc_hst_id;
		this.sns_dt_st_tp = sns_dt_st_tp;
		this.sns_dt_pr_rslt_name = sns_dt_pr_rslt_name;
		this.sns_dt_pr_rslt_content = sns_dt_pr_rslt_content;
		this.sns_rw_dt_name = sns_rw_dt_name;
		this.sns_rw_dt_content = sns_rw_dt_content;
		this.rg_time = rg_time;
		this.up_time = up_time;
	}
	
	public Data() {}
	@Override
	public String toString() {
		return "Data [rec_id=" + rec_id + ", ocr_time=" + ocr_time
				+ ", location=" + location + ", p_latitude=" + p_latitude
				+ ", p_longitude=" + p_longitude + ", sns_tp=" + sns_tp
				+ ", sns_id=" + sns_id + ", sns_op_st=" + sns_op_st
				+ ", adm_id=" + adm_id + ", acc_aut_tp=" + acc_aut_tp
				+ ", acc_hst_id=" + acc_hst_id + ", sns_dt_st_tp="
				+ sns_dt_st_tp + ", sns_dt_pr_rslt_name=" + sns_dt_pr_rslt_name
				+ ", sns_dt_pr_rslt_content=" + sns_dt_pr_rslt_content
				+ ", sns_rw_dt_name=" + sns_rw_dt_name + ", sns_rw_dt_content="
				+ sns_rw_dt_content + ", rg_time=" + rg_time + ", up_time="
				+ up_time + "]";
	}
	
}
