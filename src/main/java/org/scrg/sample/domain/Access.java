package org.scrg.sample.domain;

public class Access {
	
	private int access_hst_id;
	private String access_hst_name;
	private int access_aut_type;
	private String access_aut_type_name;
	public int getAccess_hst_id() {
		return access_hst_id;
	}
	public void setAccess_hst_id(int access_hst_id) {
		this.access_hst_id = access_hst_id;
	}
	public String getAccess_hst_name() {
		return access_hst_name;
	}
	public void setAccess_hst_name(String access_hst_name) {
		this.access_hst_name = access_hst_name;
	}
	public int getAccess_aut_type() {
		return access_aut_type;
	}
	public void setAccess_aut_type(int access_aut_type) {
		this.access_aut_type = access_aut_type;
	}
	public String getAccess_aut_type_name() {
		return access_aut_type_name;
	}
	public void setAccess_aut_type_name(String access_aut_type_name) {
		this.access_aut_type_name = access_aut_type_name;
	}
	public Access(int access_hst_id, String access_hst_name,
			int access_aut_type, String access_aut_type_name) {
		this.access_hst_id = access_hst_id;
		this.access_hst_name = access_hst_name;
		this.access_aut_type = access_aut_type;
		this.access_aut_type_name = access_aut_type_name;
	}

	public Access() {}
	
	@Override
	public String toString() {
		return "Access [access_hst_id=" + access_hst_id + ", access_hst_name="
				+ access_hst_name + ", access_aut_type=" + access_aut_type
				+ ", access_aut_type_name=" + access_aut_type_name + "]";
	}
	
}
