package org.scrg.sample.domain;

public class Sensor {

	private int sensor_id;
	private String sensor_name;
	private int sensor_type;
	private String sensor_type_name;
	private int sensor_op_status;
	private String senser_op_status_name;
	private int sensor_dt_st_type;
	private String sensor_dt_st_type_name;
	
	public int getSensor_id() {
		return sensor_id;
	}

	public void setSensor_id(int sensor_id) {
		this.sensor_id = sensor_id;
	}

	public String getSensor_name() {
		return sensor_name;
	}
	
	public void setSensor_name(String sensor_name) {
		this.sensor_name = sensor_name;
	}
	
	public int getSensor_type() {
		return sensor_type;
	}
	
	public void setSensor_type(int sensor_type) {
		this.sensor_type = sensor_type;
	}
	
	public String getSensor_type_name() {
		return sensor_type_name;
	}
	
	public void setSensor_type_name(String sensor_type_name) {
		this.sensor_type_name = sensor_type_name;
	}
	
	public int getSensor_op_status() {
		return sensor_op_status;
	}
	
	public void setSensor_op_status(int sensor_op_status) {
		this.sensor_op_status = sensor_op_status;
	}
	
	public String getSenser_op_status_name() {
		return senser_op_status_name;
	}
	
	public void setSenser_op_status_name(String senser_op_status_name) {
		this.senser_op_status_name = senser_op_status_name;
	}
	
	public int getSensor_dt_st_type() {
		return sensor_dt_st_type;
	}
	
	public void setSensor_dt_st_type(int sensor_dt_st_type) {
		this.sensor_dt_st_type = sensor_dt_st_type;
	}
	
	public String getSensor_dt_st_type_name() {
		return sensor_dt_st_type_name;
	}
	
	public void setSensor_dt_st_type_name(String sensor_dt_st_type_name) {
		this.sensor_dt_st_type_name = sensor_dt_st_type_name;
	}
	
	public Sensor(int sensor_id, String sensor_name, int sensor_type,
			String sensor_type_name, int sensor_op_status,
			String senser_op_status_name, int sensor_dt_st_type,
			String sensor_dt_st_type_name) {
		this.sensor_id = sensor_id;
		this.sensor_name = sensor_name;
		this.sensor_type = sensor_type;
		this.sensor_type_name = sensor_type_name;
		this.sensor_op_status = sensor_op_status;
		this.senser_op_status_name = senser_op_status_name;
		this.sensor_dt_st_type = sensor_dt_st_type;
		this.sensor_dt_st_type_name = sensor_dt_st_type_name;
	}

	public Sensor() {}

	@Override
	public String toString() {
		return "Sensor [sensor_id=" + sensor_id + ", sensor_name="
				+ sensor_name + ", sensor_type=" + sensor_type
				+ ", sensor_type_name=" + sensor_type_name
				+ ", sensor_op_status=" + sensor_op_status
				+ ", senser_op_status_name=" + senser_op_status_name
				+ ", sensor_dt_st_type=" + sensor_dt_st_type
				+ ", sensor_dt_st_type_name=" + sensor_dt_st_type_name + "]";
	}

}
