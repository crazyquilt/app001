package org.scrg.sample.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.security.Principal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.scrg.sample.domain.Data;
import org.scrg.sample.domain.ShowData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;

public class Manipulator {
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory
			.getLogger(Manipulator.class);
	
	private String storagePathRawdata;
	private String storagePathResult;

	private String storagePathRawdataLocal;
	private String storagePathResultLocal;
	private String Storage;

	//------------------------------
	//Strage用のアプリのための設定
	public String getStoragePathRawdata() {
		return storagePathRawdata;
	}

	public void setStoragePathRawdata(String storagePathRawdata) {
		this.storagePathRawdata = storagePathRawdata;
	}

	public String getStoragePathResult() {
		return storagePathResult;
	}

	public void setStoragePathResult(String storagePathResult) {
		this.storagePathResult = storagePathResult;
	}

	private Map<String,String> fieldName = new HashMap<String,String>();

	public Manipulator() {
		fieldName.put("ocr_time", "発生日時");
		fieldName.put("p_latitude", "緯度");
		fieldName.put("p_longitude", "経度");
		fieldName.put("sns_tp", "センサ種別");
		fieldName.put("sns_id", "センサID");
		fieldName.put("sns_op_st", "センサ動作状態");
		fieldName.put("adm_id", "管理者ID");
		fieldName.put("acc_aut_tp", "アクセス権限種別");
		fieldName.put("acc_hst_id", "アクセス履歴ID");
		fieldName.put("sns_dt_st_tp", "センサデータ状態種別");
		fieldName.put("sns_dt_pr_rslt_name", "処理結果ァイル名");
		fieldName.put("sns_dt_pr_rslt_content", "処理結果ファイルコンテントタイプ");
		fieldName.put("sns_rw_dt_name", "生データのファイル名");
		fieldName.put("sns_rw_dt_content", "生データのファイルコンテントタイプ");
		fieldName.put("rg_time", "新規登録日時");
		fieldName.put("up_time", "更新日時");
	}

	public ShowData getExif(MultipartHttpServletRequest request) 
			throws ParseException, IOException, ImageProcessingException {

		log.info("getExif:");
		Iterator<String> itr = request.getFileNames();
		MultipartFile mpf = request.getFile(itr.next());
//	    System.out.println("mpf contents: " + mpf.toString() );

//	    System.out.println(mpf.getOriginalFilename() +" is being checked!");
//	    System.out.println(mpf.getName() +" is being checked!");

		Data data = new Data();

		try {
			InputStream is = mpf.getInputStream();
			BufferedInputStream inputStream = new BufferedInputStream(is);
			Metadata metadata = ImageMetadataReader.readMetadata(inputStream, false);

			for (Directory directory : metadata.getDirectories()) {
				String result;
				// String tr_result;
				for (Tag tag : directory.getTags()) {
					result = tag.toString();
//					System.out.println("result : " + result );
					if (result.indexOf("[Exif SubIFD] Date/Time Original") != -1) {
//						System.out.println("result: " + result );
						String tr_result = dateTrimm(result);// 日付に関する文字列を整える
//						System.out.println("trimmed date : " + tr_result );
						Timestamp convertedDate = stringDateConvert(tr_result);
						data.setOcr_time(convertedDate);
					} else if (result.indexOf("GPS Latitude -") != -1) {
//						System.out.println("Latitude: " + result );
						String tr_result = latilogiTrimm(result);// 経度に関する文字列を整える
//						System.out.println("trimmed latitude : " + tr_result );
						data.setP_latitude(Double.parseDouble(tr_result));// dataにLatitudeをセットする
					} else if (result.indexOf("GPS Longitude -") != -1) {
//						System.out.println("Longitude: " + result );
						String tr_result = latilogiTrimm(result);// 緯度に関する文字列を整える
//						System.out.println("trimmed latitude : " + tr_result);
						data.setP_longitude(Double.parseDouble(tr_result));// dataにLongitudeをセットする
					}
				}
			}

		} catch (ImageProcessingException e) {
			//サポートされていない形式のファイルの場合デフォルトの値をセットする
			System.out.println("not support type");
			try {
				Long dateTimeLong = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss")
				.parse("2014:01:01 00:00:00").getTime();//デフォルトの日付をセットする
				data.setOcr_time(new Timestamp(dateTimeLong));
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
			data.setP_latitude(Double.parseDouble("36.06209214288428"));//デフォルトの緯度をセットする
			data.setP_longitude(Double.parseDouble("140.1357865333557"));//デフォルトの経度をセットする

		}

	    if (data.getOcr_time() == null){//何らかの理由で日時が取得できなかった場合
			long dateTimeLong = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss")
			.parse("2014:01:01 00:00:00").getTime();//デフォルトの日付をセットする
			data.setOcr_time(new Timestamp(dateTimeLong));
	    }

	    if (data.getP_latitude()== 0){//何らかの理由で経度が取得できなかった場合
			data.setP_latitude(Double.parseDouble("36.06209214288428"));//デフォルトの緯度をセットする
	    }

	    if (data.getP_longitude() == 0) {//何らかの理由で緯度が取得できなかった場合
			data.setP_longitude(Double.parseDouble("140.1357865333557"));//デフォルトの経度をセットする
	    }

	    // Exif情報以外の各項目にデフォルトの値をセットする
	    data.setRec_id(0);
	    data.setSns_id(0);
	    data.setAdm_id(0);
	    data.setAcc_hst_id(0);
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		data.setRg_time(currentTime);
		data.setUp_time(currentTime);
		data.setSns_dt_pr_rslt_name("");
		data.setSns_rw_dt_name("");

		ShowData showData = new ShowData();
		showData = convertToShowRest(data , request);

	    return showData;

	}

	// java.util.Date → String → long → Timestamp
	// 日付の形式をMySQL仕様に変換する
	//
	public Timestamp dateConvert(java.util.Date date) throws ParseException, ParseException {

//	    System.out.println("dateConvert today: " + date);
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    String strDate = sdf.format(date);
	    System.out.println("strDate: " + strDate);

		//dataデータにデフォルトの値をセットする
		Long dateTimeLong;
//		try {
			dateTimeLong = sdf.parse(strDate).getTime();
			Timestamp dateTime = new Timestamp(dateTimeLong);
		    System.out.println("dateCovert dateTime: " + dateTime);
		    return dateTime;
//		} catch(ParseException e) {
//		    System.out.println("parse error: " + date);
//			return null;
//		}

	}

	// String → long → Timestamp
	// String形式の日付をMySQL仕様に変換する
	//
	public Timestamp stringDateConvert(String strDate) throws ParseException {

//	    System.out.println("dateConvert strDate: " + strDate);
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");

		Long dateTimeLong;
//		try {
			dateTimeLong = sdf.parse(strDate).getTime();
			Timestamp dateTime = new Timestamp(dateTimeLong);
			return dateTime;
//		} catch (ParseException e) {
//		    System.out.println("parse error: " + strDate);
//			e.printStackTrace();
//			stringDateConvert2(strDate);
//			return null;
//		}//long形式に整える
	}

	// String → long → Timestamp
	// String形式の日付をMySQL仕様に変換する
	//
	public Timestamp stringDateConvert2(String strDate) throws ParseException {

	    System.out.println("dateConvert2 strDate: " + strDate);
//	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    SimpleDateFormat sdf = new SimpleDateFormat();

	    String strDate2 = null;
//	    try {
			java.util.Date date = sdf.parse(strDate);
		    System.out.println("converted date: " + date);
		    strDate2 = sdf.format(date);
		    System.out.println("strDate: " + strDate2);
//		} catch (ParseException e1) {
//		    System.out.println("parse error: " + e1);
//			e1.printStackTrace();
//		}
		//dataデータにデフォルトの値をセットする
		Long dateTimeLong;
//		try {
			dateTimeLong = sdf.parse(strDate2).getTime();
			Timestamp dateTime = new Timestamp(dateTimeLong);
		    System.out.println("dateCovert dateTime: " + dateTime);
		    return dateTime;
//		} catch(ParseException e) {
//		    System.out.println("parse error: " + strDate2);
//			return null;
//		}

	}

	/**
	 * 60進数の位置情報（緯度または経度）を10進数に変換する
	 *
	 * @param str　Exif情報に含まれる位置情報
	 * @return
	 */
	public String latilogiTrimm(String str) {
		String dist = "";
		int pos = str.indexOf("- ");
		// System.out.println("pos = " + pos);
		if (str.substring(pos + 2) != null) {
			dist = str.substring(pos + 2);
			// System.out.println("location : " + dist);
		}
		// 度・分・秒のデータに分解する
		dist = dist.replaceAll("°", "");
		dist = dist.replaceAll("\'", "");
		dist = dist.replaceAll("\"", "");
		// System.out.println("location : " + dist);
		String[] str1Ary = dist.split(" ");
		for (int i = 0; i < str1Ary.length; i++) {
			// System.out.println(str1Ary[i]);
		}
		double degree = Double.valueOf(str1Ary[0]);
		double minute = Double.valueOf(str1Ary[1]);
		double second = Double.valueOf(str1Ary[2]);
		// System.out.println(degree + " : " + minute + " : " + second);
		// 分と秒のデータを10進数に変換する
		// System.out.println(minute / 60);
		// System.out.println(second / (60 * 60));
		double location = degree + (minute / 60) + (second / (60 * 60));
		dist = Double.toString(location);
		// System.out.println("location : " + dist);

		return dist;
	}

	/**
	 * Exif情報に含まれる日付をデータベース用に変換する
	 *
	 * @param str　Exif情報に含まれる日付
	 * @return
	 */
	public String dateTrimm(String str) {

//		System.out.println("original : " + str);

		String dist = "";

		int pos = str.indexOf("- ");
		// System.out.println("pos = " + pos);
		if (str.substring(pos + 2) != null) {
			dist = str.substring(pos + 2);
//			System.out.println("datetime : " + dist);
		}
		return dist;

//		return null;
	}

	/**
	 * Data形式のデータをShowData形式のデータに変換する REST
	 * file URL = <a href .....> tag
	 *
	 * @param data　データベースから取得したままのデータ
	 * @param request
	 * @return
	 */
	public ShowData convertToShowRest(Data data, HttpServletRequest request) {

	    String requestURL = request.getRequestURL().toString();
//	    System.out.println("RequestURL: " + requestURL);
	    String requestURI = request.getRequestURI().toString();
//	    System.out.println("RequestURI: " + requestURI);
	    Storage = requestURL.replace(requestURI, "/Storage") ;
//	    System.out.println("Storage: " + Storage);
	    storagePathRawdataLocal = Storage + "/rawdata/";
//		System.out.println("rawPathRawdataLocal: " + storagePathRawdataLocal);
	    storagePathResultLocal = Storage + "/result/";
//		System.out.println("rawPathResultLocal: " + storagePathResultLocal);

	    ShowData showData = new ShowData();

	    showData.setRec_id(Integer.toString(data.getRec_id()));
    	String ocrtime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(data.getOcr_time());
    	String rgtime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(data.getRg_time());
    	String uptime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(data.getUp_time());

		showData.setRec_id(Integer.toString(data.getRec_id()));
		showData.setOcr_time(ocrtime);
		showData.setP_latitude(Double.toString(data.getP_latitude()));
		showData.setP_longitude(Double.toString(data.getP_longitude()));
//		showData.setLocation("POINT(" + showData.getP_longitude() + " " + showData.getP_latitude() + ")" );
		showData.setSns_id(Integer.toString(data.getSns_id()));
		showData.setAdm_id(Integer.toString(data.getAdm_id()));
		showData.setAcc_hst_id(Integer.toString(data.getAcc_hst_id()));
		showData.setSns_dt_pr_rslt_name(data.getSns_dt_pr_rslt_name());
		showData.setSns_dt_pr_rslt_content(data.getSns_dt_pr_rslt_content());
		showData.setSns_rw_dt_name(data.getSns_rw_dt_name());
		showData.setSns_rw_dt_content(data.getSns_rw_dt_content());
		showData.setRg_time(rgtime);
		showData.setUp_time(uptime);

    	String rawFileName = null;
    	String resultFileName = null;

//		RawFileName = storagePathRawdata + File.separator + data.getSns_rw_dt_name();
		rawFileName = storagePathRawdataLocal + data.getSns_rw_dt_name();
		resultFileName = storagePathResultLocal + data.getSns_dt_pr_rslt_name();
//     	System.out.println("sns_dt_pr_rslt: " + data.getSns_dt_pr_rslt_name() + "*");


		//Client用のURLをサーバーサイドで合成する
		String tmp1 = "";
		String tmp2 = null;
		tmp1 = "<a href = \"" + rawFileName + "\"  class=\"preview\" target=\"_blank\">センサ生データ</a>";

//		if (!data.getSns_dt_pr_rslt_name().isEmpty()) {
		if ( data.getSns_dt_pr_rslt_name() != null && !data.getSns_dt_pr_rslt_name().isEmpty() ) {
			tmp2 = "<a href = \"" + resultFileName + "\"  class=\"preview\" target=\"_blank\">センサデータ処理結果</a>";
		}
//     	System.out.println("tmp1: " + tmp1);
//     	System.out.println("tmp2: " + tmp2);

		showData.setSns_rw_dt_name(tmp1);
		showData.setSns_dt_pr_rslt_name(tmp2);

		return showData;

//		return null;
	}

	public int accessHistoryIdGet() {

		//現状ではデフォルトでセットするのみ
		return 2;

	}

	public int adminIdGet() {

		//現状ではデフォルトでセットするのみ
		return 3;

	}

	public int sensorIdGet() {

		//現状ではデフォルトでセットするのみ
		return 1;

	}

	public String getStoreFile(String dataFileName , HttpServletRequest request, ServletContext context ,
			Principal principal, String storagePathRawdata, String storagePathResult) {

		if (principal !=null) {
			String loggedUserName = principal.getName();
			log.info("getstorefile: " + loggedUserName);
		}
		else log.info("getStoreFile: ");

//		System.out.println("getStoreFile context= " + servletContext);

	    //カウント保持ファイルにアクセスして現在のカウントを取得
//	    String countFile = dataPath + File.separator + "count.txt";
//	    String countFile = realPath + File.separator + "count.txt";
	    String countFile = storagePathRawdata + File.separator + "count.txt";
//	    System.out.println("カウントファイル： " + countFile);
	    log.info("カウントファイル： " + countFile);
	    File cFile = new File(countFile);
		FileInputStream fis = null;
		int fileCount = 0;
	    try {

//	    	FileReader cFileReader = new FileReader(cFile);
			fis = new FileInputStream(cFile);
			
		} catch (FileNotFoundException e) {//ファイルがない場合作成してカウントをゼロにする
		    System.out.println(e);
			try {
			    System.out.println("create file");
				cFile.createNewFile();
				File outputFile = new File(countFile);
				FileOutputStream fos = new FileOutputStream(outputFile);
				OutputStreamWriter osw = new OutputStreamWriter(fos);
				PrintWriter pw = new PrintWriter(osw);
				
//			    System.out.println("write count");
				pw.println(0);
				
				pw.close();
				osw.close();
				fos.close();

				fis = new FileInputStream(cFile);

			} catch (IOException e1) {//IO Exception
			    System.out.println(e1);
			}
		}
	    
//	    System.out.println("fis: " + fis);
	    

	    try {
//			FileInputStream fis = new FileInputStream(cFile);
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);
			
			String msg;
			if( (msg=br.readLine())!=null){
//				System.out.println("msg: " + msg);
			}
			fileCount = Integer.parseInt(msg);
//			System.out.println("prevCount: " + fileCount);

			br.close();
			
		} catch (FileNotFoundException e) {//ファイルがない場合
		    System.out.println(e);
			e.printStackTrace();
		} catch (IOException e) {//IO Exception
		    System.out.println(e);
			e.printStackTrace();
		}
	    
	    //カウントを更新
		fileCount ++;
//		System.out.println("nextCount: " + fileCount);

		//カウント保持ファイルにアクセスしてカウントを格納
		File outputFile = new File(countFile);
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(outputFile);
			OutputStreamWriter osw = new OutputStreamWriter(fos);
			PrintWriter pw = new PrintWriter(osw);
			pw.println(fileCount);
			
			pw.close();
		    
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
/*
	    int fileCount = dataPath.listFiles().length;
	    System.out.println("ファイル数： " + fileCount);
	    
	    fileCount ++;
*/
//		Manipulator manipulator = new Manipulator();
		String ext = getExtension(dataFileName);
//		System.out.println("extention: " + ext );
//	    String storeFile = dataDir + String.format("%1$05d", fileCount) + "." + ext;
//	    String storeFile = dataDir + String.format("%1$05d", fileCount) + "." + ext;
//	    String storeFile = realPath + File.separator + String.format("%1$05d", fileCount) + "." + ext;
	    String storeFile = String.format("%1$05d", fileCount) + "." + ext;
//		System.out.println("storeFile: " + storeFile );

		return storeFile;

	}

	/**
	 * ファイルの拡張子を取得する
	 * 
	 * @param filePath
	 * @return
	 */
	public String getExtension(File filePath) {
	    // パス名が示すディレクトリが存在する場合はnullを返します。
	    if (filePath.isDirectory()) {
	        return null;
	    }

	    // パス名が示すファイルの名前を返します。
	    String fileName = filePath.getName();

	    return getExtension(fileName);
	}

	/**
	 * ファイルの拡張子を取得する　次にファイル名として取り扱う
	 * 
	 * @param fileName
	 * @return
	 */
	public String getExtension(String fileName) {
	    if (fileName == null) {
	        return null;
	    }
	 
	    // 最後の『 . 』の位置を取得します。
	    int lastDotPosition = fileName.lastIndexOf(".");

	    // 『 . 』が存在する場合は、『 . 』以降を返します。
	    if (lastDotPosition != -1) {
	        return fileName.substring(lastDotPosition + 1);
	    }
	    return null;
	}

	public Data convertToData(ShowData showData) {
		int recid;
	    Timestamp ocrtime;
	    double platitude;
	    double plongitude;
	    int snstp;
	    int snsid;
	    int snsopst;
	    int admid;
	    int accauttp;
	    int acchstid;
	    int snsdtsttp;
		Timestamp rgtime;
	    Timestamp uptime;

//		System.out.println("getOcr_time: " + showData.getOcr_time());

	    if (showData.getRec_id() == null){
	    	recid = 0;
	    } else {
		    recid = Integer.parseInt(showData.getRec_id());
	    }
	    
		Long dateTimeLong = null;
		try {
			dateTimeLong = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(showData.getOcr_time()).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}

//		System.out.println("dateTimeLong: " + dateTimeLong);

		ocrtime = new Timestamp(dateTimeLong);
		platitude = Double.parseDouble(showData.getP_latitude());
		plongitude = Double.parseDouble(showData.getP_longitude());
		snstp = Integer.parseInt(showData.getSns_tp());
		snsid = Integer.parseInt(showData.getSns_id());
		snsopst = Integer.parseInt(showData.getSns_op_st());
		admid = Integer.parseInt(showData.getAdm_id());
		accauttp = Integer.parseInt(showData.getAcc_aut_tp());
		acchstid = Integer.parseInt(showData.getAcc_hst_id());
		snsdtsttp = Integer.parseInt(showData.getSns_dt_st_tp());
		try {
			dateTimeLong = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(showData.getRg_time()).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		rgtime = new Timestamp(dateTimeLong);
		try {
			dateTimeLong = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(showData.getUp_time()).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		uptime = new Timestamp(dateTimeLong);

		Data data = new Data();
		data.setRec_id(recid);
		data.setOcr_time(ocrtime);
//		System.out.println("ocr_time: " + data.getOcr_time());
		data.setLocation("POINT (" + showData.getP_longitude() + " " + showData.getP_latitude() + ")");
//		System.out.println("location: " + data.getLocation());
		data.setP_latitude(platitude);
		data.setP_longitude(plongitude);
		data.setSns_tp(snstp);
		data.setSns_id(snsid);
		data.setSns_op_st(snsopst);
		data.setAdm_id(admid);
		data.setAcc_aut_tp(accauttp);
		data.setAcc_hst_id(acchstid);
		data.setSns_dt_st_tp(snsdtsttp);
		data.setSns_dt_pr_rslt_name(showData.getSns_dt_pr_rslt_name());
//		System.out.println("result name: " + data.getSns_dt_pr_rslt_name());
		data.setSns_dt_pr_rslt_content(showData.getSns_dt_pr_rslt_content());
//		System.out.println("result content: " + data.getSns_dt_pr_rslt_content());
		data.setSns_rw_dt_name(showData.getSns_rw_dt_name());
		data.setSns_rw_dt_content(showData.getSns_rw_dt_content());
//		System.out.println("rw_dt content: " + data.getSns_rw_dt_content());
		data.setRg_time(rgtime);
		data.setUp_time(uptime);
//		System.out.println("up_time: " + data.getUp_time());
		
		return data;
		
	}

	public ShowData convertToShowMobile(Data data, HttpServletRequest request) {
		
	    String requestURL = request.getRequestURL().toString();
//	    System.out.println("RequestURL: " + requestURL);
	    String requestURI = request.getRequestURI().toString();
//	    System.out.println("RequestURI: " + requestURI);
	    Storage = requestURL.replace(requestURI, "/Storage") ;
//	    System.out.println("Storage: " + Storage);
	    storagePathRawdataLocal = Storage + "/rawdata/";
//		System.out.println("rawPathRawdataLocal: " + storagePathRawdataLocal);
	    storagePathResultLocal = Storage + "/result/";
//		System.out.println("rawPathResultLocal: " + storagePathResultLocal);

	    ShowData showData = new ShowData();
	    
	    showData.setRec_id(Integer.toString(data.getRec_id()));
    	String ocrtime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(data.getOcr_time());
    	String rgtime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(data.getRg_time());
    	String uptime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(data.getUp_time());

		showData.setRec_id(Integer.toString(data.getRec_id()));
		showData.setOcr_time(ocrtime);
		showData.setP_latitude(Double.toString(data.getP_latitude()));
		showData.setP_longitude(Double.toString(data.getP_longitude()));
//		showData.setLocation("POINT(" + showData.getP_longitude() + " " + showData.getP_latitude() + ")" );
		showData.setSns_tp(Integer.toString(data.getSns_tp()));
		showData.setSns_id(Integer.toString(data.getSns_id()));
		showData.setSns_op_st(Integer.toString(data.getSns_op_st()));
		showData.setAdm_id(Integer.toString(data.getAdm_id()));
		showData.setAcc_aut_tp(Integer.toString(data.getAcc_aut_tp()));
		showData.setAcc_hst_id(Integer.toString(data.getAcc_hst_id()));
		showData.setSns_dt_st_tp(Integer.toString(data.getSns_dt_st_tp()));
		showData.setSns_dt_pr_rslt_name(data.getSns_dt_pr_rslt_name());
		showData.setSns_dt_pr_rslt_content(data.getSns_dt_pr_rslt_content());
		showData.setSns_rw_dt_name(data.getSns_rw_dt_name());
		showData.setSns_rw_dt_content(data.getSns_rw_dt_content());
		showData.setRg_time(rgtime);
		showData.setUp_time(uptime);

    	String rawFileName = null;
    	String resultFileName = null;

//		RawFileName = storagePathRawdata + File.separator + data.getSns_rw_dt_name();
		rawFileName = storagePathRawdataLocal + data.getSns_rw_dt_name();
		resultFileName = storagePathResultLocal + data.getSns_dt_pr_rslt_name();
//     	System.out.println("sns_dt_pr_rslt: " + data.getSns_dt_pr_rslt_name() + "*");

		
		//Client用のURLをサーバーサイドで合成する
		String tmp1 = "";
		String tmp2 = "";
		String tmp3 = "";
//		tmp1 = "<a href = \"" + rawFileName + "\"  class=\"preview\" rel = \"" 
		tmp1 = "<a href = \"" + rawFileName + "\"  class=\"preview\" target=\"_blank\">センサ生データ</a>";
//		if (!data.getSns_dt_pr_rslt_name().isEmpty()) {
		if ( data.getSns_dt_pr_rslt_name() != null && !data.getSns_dt_pr_rslt_name().isEmpty() ) {
//			tmp2 = "<a href = \"" + resultFileName + "\"  class=\"preview\" rel = \""
			tmp2 = "<a href = \"" + resultFileName + "\"  class=\"preview\" target=\"_blank\">センサデータ処理結果</a>";
		}
	    tmp3 = rawFileName;
//     	System.out.println("tmp1: " + tmp1);
//     	System.out.println("tmp2: " + tmp2);
		
		showData.setSns_rw_dt_name(tmp1);
		showData.setSns_dt_pr_rslt_name(tmp2);
		showData.setImage_url(tmp3);

		return showData;

//		return null;
	}

}
