package org.scrg.sample.config;

import javax.servlet.ServletContext;

import org.scrg.sample.domain.SearchCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {
	
	@Autowired
	ServletContext context;

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory
			.getLogger(AppConfiguration.class);
	
	@Bean
	public String storagePathRawdata() {
		String osname = System.getProperty("os.name");
		String storageRaw = null;

		if (osname.indexOf("Windows") >= 0) {
			// Windowsの処理　開発環境
//			String realPath = context.getRealPath("/");
			String realPath = context.getRealPath("/");
			log.info("realPath: " + realPath );
			if (realPath == null) { // in case of test
				storageRaw = "C:\\Users\\枝美子\\Documents\\workspace-sts-3.6.0.M1\\Storage\\rawdata";
			} else if ( realPath.indexOf("tomcat") != -1 ){	//localのtomcatの場合
//				storageRaw = "C:\\xampp\\tomcat\\webapps\\Storage\\rawdata";
				storageRaw = "C:\\tomcat\\webapps\\Storage\\rawdata";
			} else if ( realPath.indexOf("webapp") != -1 ){//STS上の場合
				log.info("servletcontext.realpath= " + realPath);
//				System.out.println("servletcontext.contextPath= " + context.getContextPath());
//				System.out.println("request.contextPath= " + request.getContextPath());

				int len2 = "src\\main\\webapp\\".length();
//				System.out.println("length= " + len2);
				int found = realPath.indexOf("src\\main\\webapp\\");
//				String base = realPath.substring(0, found + len2);
				String base = realPath.substring(0, ( found - 1 ) );
//				System.out.println("base= " + base);
				//base後ろの\\の位置を取得する
				int foundBack = base.lastIndexOf("\\");
				String baseBack = base.substring(0 , (foundBack + 1 ) );
//				System.out.println("baseBack= " + baseBack);
//				storageRaw = base + "Storage\\rawdata";
				storageRaw = baseBack + "Storage\\rawdata";
				// System.out.println("raw= " + StorageRaw);
			} else if ( realPath.indexOf("wtpwebapps") != -1 ){	//Eclipse上の場合
//				System.out.println("servletcontext.realpath= " + realPath);
				System.out.println("servletcontext.contextPath= " + context.getContextPath());
//				System.out.println("request.contextPath= " + request.getContextPath());

				int len2 = "wtpwebapps\\".length();
				// System.out.println("length= " + len2);
				int found = realPath.indexOf("wtpwebapps\\");
				String base = realPath.substring(0, found + len2);
				// System.out.println("base= " + base);
				storageRaw = base + "Storage\\rawdata";
				// System.out.println("raw= " + StorageRaw);
			}

		} else if (osname.indexOf("Linux") >= 0) {
			// Linuxの処理　本番環境
			// logger.info("Linux");
			// センサ生データ用フォルダのパス
			storageRaw = "/usr/local/tomcat/webapps/Storage/rawdata";
			// System.out.println("rawPath: " + storagePathRawdata);
			// センサデータ処理結果用フォルダのパス
		} else if (osname.indexOf("Mac") >= 0) {
			// MacOSであったときの処理
		} else {
			// その他の環境だったときの処理
		}
		
		log.info("rawPath: " + storageRaw);
		return storageRaw;
	}
	
	@Bean
	public String storagePathResult() {

		log.info("appconfig result:");

		String osname = System.getProperty("os.name");
		String storageResult = null;

		if (osname.indexOf("Windows") >= 0) {
			// Windowsの処理　開発環境
			String realPath = context.getRealPath("/");
			log.info("realPath: " + realPath );
			if (realPath == null) {
				storageResult = "C:\\Users\\枝美子\\Documents\\workspace-sts-3.6.0.M1\\Storage\\result";
			} else if ( realPath.indexOf("tomcat") != -1 ){	//localのtomcatの場合
//				storageResult = "C:\\xampp\\tomcatwebapps\\Storage\\result";
				storageResult = "C:\\tomcatwebapps\\Storage\\result";
			} else if ( realPath.indexOf("webapp") != -1 ){
				System.out.println("servletcontext.realpath= " + realPath);
				System.out.println("servletcontext.contextPath= " + context.getContextPath());
//				System.out.println("request.contextPath= " + request.getContextPath());

				int len2 = "src\\main\\webapp\\".length();
				System.out.println("length= " + len2);
				int found = realPath.indexOf("src\\main\\webapp\\");
//				String base = realPath.substring(0, found + len2);
				String base = realPath.substring(0, ( found - 1 ) );
				System.out.println("base= " + base);
				//base後ろの\\の位置を取得する
				int foundBack = base.lastIndexOf("\\");
				String baseBack = base.substring(0 , (foundBack + 1 ) );
				System.out.println("baseBack= " + baseBack);
//				storageRaw = base + "Storage\\rawdata";
				storageResult = baseBack + "Storage\\result";
				// System.out.println("raw= " + StorageRaw);
			} else if ( realPath.indexOf("wtpwebapps") != -1 ){
				int len2 = "wtpwebapps\\".length();
				// System.out.println("length= " + len2);
				int found = realPath.indexOf("wtpwebapps\\");
				String base = realPath.substring(0, found + len2);
				// System.out.println("base= " + base);
				storageResult = base + "Storage\\result";
				// System.out.println("result= " + StorageResult);
			} 

		} else if (osname.indexOf("Linux") >= 0) {
			// Linuxの処理　本番環境
			// logger.info("Linux");
			// センサデータ処理結果用フォルダのパス
			storageResult = "/usr/local/tomcat/webapps/Storage/result";
			// System.out.println("resultPath: " + storagePathResult);
		} else if (osname.indexOf("Mac") >= 0) {
			// MacOSであったときの処理
		} else {
			// その他の環境だったときの処理
		}
		System.out.println("resultPath: " + storageResult);
		return storageResult;
	}
	
	// 検索条件　保管用のクラス
	@Bean
	SearchCriteria searchCriteria() {
		SearchCriteria searchCriteria = new SearchCriteria("0" , "0" , "0" , "" , "" , "" , "" , "" , "" , "" , "straight" , "" , false );
		return searchCriteria;
	}
	
	// 検索条件　ファイルタイプ
	@Bean
	String[] contentsTypes() {
		
		String [] con = { "" , "image","audio","video","application" };
		return con;
	}
	
	// 検索条件　検索タイプ
	@Bean
	String[] searchTypes() {

		String[] sear = { "straight" , "rect" , "circle" };
		return sear;
	}
	
	// ファイルの一時置き場
	@Bean
	String tempPath () {
		return "temp/";
	}
	
	// デフォルトの日時
	@Bean
	String defaultDate() {
		return "2014:01:01 00:00:00";
	}
	
	// デフォルトのlatitude
	@Bean
	String defaultLatitude() {
		return "36.06209214288428";
	}
	
	// デフォルトのlongitude
	@Bean
	String defaultLongitude() {
		return "140.1357865333557";
	}
	
	// 1秒当たりの度
	@Bean
	String degreePerSecond() {
		return "0.000277778";
	}
	
	// 1秒あたりの距離（メートル） latitude
	@Bean
	String meterPerSecondLat() {
		return "30.8184";
	}
	
	// 1秒あたりの距離（メートル） longitude
	@Bean
	String meterPerSecondLng() {
		return "25.153129";
	}
	
	// 1度当たりの距離（メートル）　latitude
	@Bean
	String distancePerDegreeLat() {
		return "110946";
	}
	
	// 1度当たりの距離（メートル）　longitude
	@Bean
	String distancePerDegreeLng() {
		return "90551";
	}
	
	// 1ページに表示するデータ（行）数
	@Bean
	String linePerPage() {
		return "5";
	}
	
	// REST 1ページに表示するデータ(行）数
	@Bean
	String linePerPageREST() {
		return "25";
	}
	
}
