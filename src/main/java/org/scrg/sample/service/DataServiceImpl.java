package org.scrg.sample.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.scrg.sample.dao.DataDao;
import org.scrg.sample.domain.Data;
import org.scrg.sample.domain.LocationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataServiceImpl implements DataService {
	
	@Autowired
	DataDao dataDao;
	
	//InputStreamをStorage/rawに書き込む
	@Override
	public void rawFileWrite(InputStream is, String rawFileName) {
		dataDao.rawFileWrite(is, rawFileName);
	}

	//データを新たに登録する
	@Override
	public String addData(Data data) {
		return dataDao.addData(data);
	}
	
	//queryを指定して、円形検索を行いデータを取得する params
	@Override
	public List<Data> getDataListByQueryCircleWithParams( String query , String lng1, String lat1, String rad ,
			Object[] params ) {
		
		return new ArrayList<Data>();
	}

	//queryを指定して、円形検索を行いデータ数を取得する params
	@Override
	public int getDataCountByQueryCircleWithParams( String query , String lng1, String lat1, String rad,
			Object[] params ) {
		
		return 1;
	}
	
	//queryを指定して、円形検索を行いデータを取得する without params
	@Override
	public List<Data> getDataListByQueryCircleWithoutParams( String query , String lng1, String lat1, String rad ) {
		
		return this.dataDao.getDataListByQueryCircleWithoutParams(query, lng1, lat1, rad);

	}

	//queryを指定して、円形検索を行いデータ数を取得する without params
	@Override
	public int getDataCountByQueryCircleWithoutParams( String query , String lng1, String lat1, String rad ) {
		
		return this.dataDao.getDataCountByQueryCircleWithoutParams(query, lng1, lat1, rad);
		
	}
	
	//queryを指定してデータのカウントを取得する with params 
	@Override
	public int getDataCountByQueryWithParams( String query , Object[] values ) {
		return 1;
	}

	//queryを指定してデータのカウントを取得する without params
	@Override
	public int getDataCountByQueryWithoutParams( String query ) {
		
		return this.dataDao.getDataCountByQueryWithoutParams(query);
	}

	//queryを指定してデータを取得する with params
	@Override
	public List<Data> getDataListByQueryWithParams( String query , Object[] values ) {
		return new ArrayList<Data>();
	}

	//queryを指定してデータを取得する without params
	@Override
	public List<Data> getDataListByQueryWithoutParams( String query ) {
		return this.dataDao.getDataListByQueryWithoutParams(query);
	}

	//queryを指定して、緯度と経度を取得する with params
	@Override
	public List<LocationData> getLocationDataByQueryWithParams( String query , Object[] values ) {
		return this.dataDao.getLocationDataByQueryWithParams(query, values);
	}

	//queryを指定して、緯度と経度を取得する without params
	@Override
	public List<LocationData> getLocationDataByQueryWithoutParams( String query ) {
		return this.dataDao.getLocationDataByQueryWithoutParams(query);

	}

	//queryを指定して、データを取得する
	public Data getDataByQuery( String query , Object[] values) {
		return this.dataDao.getDataByQuery(query, values);
	}

}
