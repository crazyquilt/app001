package org.scrg.sample.service;

import java.util.List;

import org.scrg.sample.dao.AdminDao;
import org.scrg.sample.domain.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {
	
	@Autowired
	AdminDao adminDao;

	//すべてのAdminを取得する
	public List<Admin> findAll() {
		
		return this.adminDao.findAll();
		
	}

	//管理者IDからデータを取得する
	public Admin findById(int id) {
		
		return this.adminDao.findById(id);
	}

}
