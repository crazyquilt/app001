package org.scrg.sample.service;

import java.util.List;

import org.scrg.sample.domain.Admin;

public interface AdminService {

	//すべてのAdminを取得する
	public List<Admin> findAll();

	//管理者IDからデータを取得する
	public Admin findById(int id);

}
