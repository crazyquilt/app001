package org.scrg.sample.service;

import java.util.List;

import org.scrg.sample.dao.AccessDao;
import org.scrg.sample.domain.Access;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccessServiceImpl implements AccessService {
	
	@Autowired
	AccessDao accessDao;

	//すべてのaccessを取得する
	public List<Access> findAll() {
		
		return this.accessDao.findAll();
	}

	//アクセス履歴IDからデータを取得する
	public Access findById(int id) {

		return this.accessDao.findById(id);
	}

}
