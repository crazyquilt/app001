package org.scrg.sample.service;

import java.util.List;

import org.scrg.sample.dao.SensorDao;
import org.scrg.sample.domain.Sensor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SensorServiceImpl implements SensorService {
	
	@Autowired
	SensorDao sensorDao;
	
	//すべてのSensorを取得する
	public List<Sensor> findAll() {
		
		return this.sensorDao.findAll();
	}

	//センサIDからデータを取得する
	public Sensor findById(int id) {

		return this.sensorDao.findById(id);
	}

}
