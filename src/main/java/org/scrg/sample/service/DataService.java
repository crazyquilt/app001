package org.scrg.sample.service;

import java.io.InputStream;
import java.util.List;

import org.scrg.sample.domain.Data;
import org.scrg.sample.domain.LocationData;

public interface DataService {

	//InputStreamをStorage/rawに書き込む
	public void rawFileWrite(InputStream is, String rawFileName);

	//データを新たに登録する
	public String addData(Data data);
	
	//queryを指定して、円形検索を行いデータを取得する params
	public List<Data> getDataListByQueryCircleWithParams( String query , String lng1, String lat1, String rad ,
			Object[] params );

	//queryを指定して、円形検索を行いデータ数を取得する params
	public int getDataCountByQueryCircleWithParams( String query , String lng1, String lat1, String rad,
			Object[] params );
	
	//queryを指定して、円形検索を行いデータを取得する without params
	public List<Data> getDataListByQueryCircleWithoutParams( String query , String lng1, String lat1, String rad );

	//queryを指定して、円形検索を行いデータ数を取得する without params
	public int getDataCountByQueryCircleWithoutParams( String query , String lng1, String lat1, String rad );
	
	//queryを指定してデータのカウントを取得する with params 
	public int getDataCountByQueryWithParams( String query , Object[] values );

	//queryを指定してデータのカウントを取得する without params
	public int getDataCountByQueryWithoutParams( String query );

	//queryを指定してデータを取得する with params
	public List<Data> getDataListByQueryWithParams( String query , Object[] values );

	//queryを指定してデータを取得する without params
	public List<Data> getDataListByQueryWithoutParams( String query );

	//queryを指定して、緯度と経度を取得する with params
	public List<LocationData> getLocationDataByQueryWithParams( String query , Object[] values ) ;

	//queryを指定して、緯度と経度を取得する without params
	public List<LocationData> getLocationDataByQueryWithoutParams( String query ) ;

	//queryを指定して、データを取得する
	public Data getDataByQuery( String query , Object[] values);

}
