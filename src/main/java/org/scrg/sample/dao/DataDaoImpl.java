package org.scrg.sample.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.sql.DataSource;

import org.scrg.sample.domain.Data;
import org.scrg.sample.domain.LocationData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class DataDaoImpl implements DataDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	DataSource dataSource;

	@Autowired
	ServletContext servletContext;

	@Autowired
	ServletConfig servletConfig;
	
	static private String tableName = "test003";//テーブル名

	static final String SQL_DETAIL = "SELECT REC_ID, OCR_TIME,ASTEXT(LOCATION) LOCATION, x(location) as p_longitude, "
			+ "y(location) as p_latitude , SNS_TP,SNS_ID,SNS_OP_ST,ADM_ID,ACC_AUT_TP,"
			+ "ACC_HST_ID,SNS_DT_ST_TP,RG_TIME, UP_TIME , SNS_DT_PR_RSLT_NAME , SNS_RW_DT_CONTENT , SNS_RW_DT_NAME "
			+ "from ";//全項目取得

static final String SQL_OUTLINE = "SELECT REC_ID, OCR_TIME ,ASTEXT(LOCATION) location, "
		+ "x(location) as p_longitude, y(location) as p_latitude , "
		+ "sns_dt_pr_rslt_name , sns_rw_dt_name from ";//一部の項目取得

static final String SQL_LOCATION = "SELECT REC_ID , ASTEXT(LOCATION) location, "
		+ "x(location) as p_longitude, y(location) as p_latitude , SNS_RW_DT_CONTENT from ";//緯度と経度、contentTypeのみ取得

static final String SQL_COUNT = "SELECT COUNT(*) cnt from ";//カウント取得

static final String SQL_RW_DT_NAME = "SELECT SNS_RW_DT_NAME from ";//生データのファイル名を取得

static final String SQL_RSLT_NAME = "SELECT SNS_DT_PR_RSLT_NAME from ";//結果のファイル名を取得

static final String SQL_RW_DT_CONTENT = "SELECT SNS_RW_DT_CONTENT from ";//生データのcontentTypeを取得

static final String SQL_RSLT_CONTENT = "SELECT SNS_DT_PR_RSLT_CONTENT from ";//結果のcontentTypeを取得

static final String SQL_INSERT = "INSERT into ";//挿入

static final String SQL_DELETE="UPDATE ";//削除 実際はenabledを0にする処理　データそのものは残っている

static final String SQL_UPDATE="UPDATE ";//更新

static final String SQL_ENABLED = " where enabled = 1 ";//enabled

static final String SQL_ID = " AND rec_id = ? ";//idの一致

static final String SQL_PAGE = " limit  ? , ? ";//ページ送り

static final String SQL_LOCATION_START = " AND MBRContains(GeomFromText("
        	+ "\'LineString(";//位置情報開始

static final String SQL_LOCATION_END = " )\'), LOCATION)";//位置情報終了

static final String SQL_GLENGTH_START = "SELECT GLength(GeomFromText('LineString(";//距離開始

static final String SQL_GLENGTH_END = " )'))";//距離終了

static final String SQL_ORDER_DATE = " order by ocr_time ";//日付順

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory
			.getLogger(DataDaoImpl.class);
	
	//InputStreamをStorage/rawに書き込む
	@Override
	public void rawFileWrite(InputStream is, String rawFileName) {
		log.info("raw file write: ");

		FileOutputStream fos = null;
    	try {
//			ImageInputStream iis = ImageIO.createImageInputStream(is);
			File file = new File(rawFileName);
			try {
				fos = new FileOutputStream(file);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			byte[] buffer = new byte[1024];
			int length=0;
			
			while((length = is.read(buffer))>= 0) {
				fos.write(buffer, 0, length);
			}
			
			fos.close();
			fos=null;
			
		} catch (IOException e) {
		}

	}

	//データを新たに登録する
	@Override
	public String addData(Data data) {
		log.info("adddata: ");

		Object[] params = new Object[] { data.getOcr_time(), data.getLocation(), data.getSns_tp(),
        		data.getSns_id(), data.getSns_op_st(), data.getAdm_id() , data.getAcc_aut_tp() ,
        		data.getAcc_hst_id() , data.getSns_dt_st_tp() , 
        		data.getRg_time(), data.getUp_time() , data.getSns_rw_dt_content() , data.getSns_rw_dt_name() , 1 };

		/*
    	String sql = "INSERT into " + tableName + " (ocr_time, location , sns_tp , sns_id , sns_op_st , "
   			+ "adm_id , acc_aut_tp , acc_hst_id , sns_dt_st_tp , "
   			+ "rg_time , up_time , sns_rw_dt_content , sns_rw_dt_name , enabled) "
   			+ "VALUES (?,GeomFromText(?),?,?,?,?,?,?,?,?,?,?,?,?)";
		*/

    	String sql = SQL_INSERT + tableName + " (ocr_time, location , sns_tp , sns_id , sns_op_st , "
       			+ "adm_id , acc_aut_tp , acc_hst_id , sns_dt_st_tp , "
       			+ "rg_time , up_time , sns_rw_dt_content , sns_rw_dt_name , enabled) "
       			+ "VALUES (?,GeomFromText(?),?,?,?,?,?,?,?,?,?,?,?,?)";

		log.info("sql: " + sql );

//		System.out.println("adding data sql: " + sql);
//		System.out.println("adding data ocr_time: " + data.getOcr_time());
//		System.out.println("adding data location: " + data.getLocation());
//		System.out.println("adding data sns_tp: " + data.getSns_tp());
//		System.out.println("adding data sns_id: " + data.getSns_id());
//		System.out.println("adding data sns_op_st: " + data.getSns_op_st());
//		System.out.println("adding data adm_id: " + data.getAdm_id());
//		System.out.println("adding data aut_tp: " + data.get_aut_tp());
//		System.out.println("adding data _hst_id: " + data.get_hst_id());
//		System.out.println("adding data sns_dt_st_tp: " + data.getSns_dt_st_tp());
//		System.out.println("adding data rg_time: " + data.getRg_time());
//		System.out.println("adding data up_time: " + data.getUp_time());
//		System.out.println("adding data content: " + data.getSns_rw_dt_content());
//		System.out.println("adding data file name: " + data.getSns_rw_dt_name());

    	jdbcTemplate.update(sql, params);

		return "ok";

	}

	//queryを指定して、円形検索を行いデータを取得する params
	@Override
	public List<Data> getDataListByQueryCircleWithParams( String query , String ln1, String la1, String rad ,
			Object[] params ) {
	
		String sql = SQL_OUTLINE + tableName + SQL_ENABLED + query;
   		log.info("getDataListByQueryCircleWithParams: " + sql );

        RowMapper<Data> mapper = new BeanPropertyRowMapper<Data>(Data.class);
        List<Data> tmp_data = jdbcTemplate.query(sql, params , mapper);

        List<Data> rtn_data = new ArrayList<Data>();
        for  (Data calc : tmp_data){
        	//地点1の緯度経度（データベース上のデータ）
        	double lat1 = calc.getP_latitude();
        	double lng1 = calc.getP_longitude();
        	//地点2の緯度経度（検索中心）
        	double lat2 = Double.parseDouble( la1 );
        	double lng2 = Double.parseDouble(ln1);

        	//平均緯度
        	double latAvg = ((lat1 + ( (lat2 - lat1)/2) ) /180) * Math.PI;
        	//緯度差
        	double latDifference = (( lat1 - lat2 ) / 180 ) * Math.PI;
        	//経度差
        	double lonDifference = (( lng1 - lng2 ) / 180) * Math.PI;

        	double curRadiusTemp = 1 - 0.00669438 * Math.pow(Math.sin(latAvg), 2);
        	
        	//子午線曲率半径
        	double meridianCurvatureRadius = 6335439.327 / Math.sqrt(Math.pow(curRadiusTemp, 3));

        	//卯酉線曲率半径
        	double primeVerticalCircleCurbatureRaduis = 6378137 / Math.sqrt(curRadiusTemp);

        	//2点間の距離
        	double distance = Math.pow(meridianCurvatureRadius * latDifference, 2) +
        			Math.pow(primeVerticalCircleCurbatureRaduis * Math.cos(latAvg) * lonDifference , 2);
        	distance = Math.sqrt(distance);
        	
        	
           	if (distance <= Double.parseDouble(rad)){
//        		System.out.println("under: ");
        		rtn_data.add(calc);
        	}
        	else {
//        		System.out.println("over: ");

        	}
        }
        return rtn_data;

	}

	//queryを指定して、円形検索を行いデータ数を取得する params
	@Override
	public int getDataCountByQueryCircleWithParams( String query , String ln1, String la1, String rad,
			Object[] params ) {
		
		String sql = SQL_OUTLINE + tableName + SQL_ENABLED + query;
   		log.info("getCountDataByQueryCircleWithParams: " + sql );

        RowMapper<Data> mapper = new BeanPropertyRowMapper<Data>(Data.class);
        List<Data> tmp_data = jdbcTemplate.query(sql, params , mapper);

        int count = 0;
        for  (Data calc : tmp_data){
//        	System.out.println("rec_id: " + calc.getRec_id());
        	//地点1の緯度経度（データベース上のデータ）
        	double lat1 = calc.getP_latitude();
        	double lng1 = calc.getP_longitude();
        	//地点2の緯度経度（検索中心）
        	double lat2 = Double.parseDouble( la1 );
        	double lng2 = Double.parseDouble(ln1);

        	//平均緯度
        	double latAvg = ((lat1 + ( (lat2 - lat1)/2) ) /180) * Math.PI;
        	//緯度差
        	double latDifference = (( lat1 - lat2 ) / 180 ) * Math.PI;
        	//経度差
        	double lonDifference = (( lng1 - lng2 ) / 180) * Math.PI;

        	double curRadiusTemp = 1 - 0.00669438 * Math.pow(Math.sin(latAvg), 2);
        	
        	//子午線曲率半径
        	double meridianCurvatureRadius = 6335439.327 / Math.sqrt(Math.pow(curRadiusTemp, 3));

        	//卯酉線曲率半径
        	double primeVerticalCircleCurbatureRaduis = 6378137 / Math.sqrt(curRadiusTemp);

        	//2点間の距離
        	double distance = Math.pow(meridianCurvatureRadius * latDifference, 2) +
        			Math.pow(primeVerticalCircleCurbatureRaduis * Math.cos(latAvg) * lonDifference , 2);
        	distance = Math.sqrt(distance);
//    		System.out.println("distance: " + distance);
        	if (distance <= Double.parseDouble(rad)){
//       		System.out.println("under: ");
        		count++;
        	}
        	else {
//        		System.out.println("over: ");
        	}
        }
        return count;

	}
	
	//queryを指定して、円形検索を行いデータを取得する without params
	@Override
	public List<Data> getDataListByQueryCircleWithoutParams( String query , String ln1, String la1, String rad ) {
		
		String sql = SQL_OUTLINE + tableName + SQL_ENABLED + query;
   		log.info("getDataListByQueryCircleWithoutParams: " + sql );

        RowMapper<Data> mapper = new BeanPropertyRowMapper<Data>(Data.class);
        List<Data> tmp_data = jdbcTemplate.query(sql, mapper);

        List<Data> rtn_data = new ArrayList<Data>();
        
        for  (Data calc : tmp_data){

        	//地点1の緯度経度（データベース上のデータ）
        	double lat1 = calc.getP_latitude();
        	double lng1 = calc.getP_longitude();
        	//地点2の緯度経度（検索中心）
        	double lat2 = Double.parseDouble( la1 );
        	double lng2 = Double.parseDouble(ln1);

        	//平均緯度
        	double latAvg = ((lat1 + ( (lat2 - lat1)/2) ) /180) * Math.PI;
        	//緯度差
        	double latDifference = (( lat1 - lat2 ) / 180 ) * Math.PI;
        	//経度差
        	double lonDifference = (( lng1 - lng2 ) / 180) * Math.PI;

        	double curRadiusTemp = 1 - 0.00669438 * Math.pow(Math.sin(latAvg), 2);
        	
        	//子午線曲率半径
        	double meridianCurvatureRadius = 6335439.327 / Math.sqrt(Math.pow(curRadiusTemp, 3));

        	//卯酉線曲率半径
        	double primeVerticalCircleCurbatureRaduis = 6378137 / Math.sqrt(curRadiusTemp);

        	//2点間の距離
        	double distance = Math.pow(meridianCurvatureRadius * latDifference, 2) +
        			Math.pow(primeVerticalCircleCurbatureRaduis * Math.cos(latAvg) * lonDifference , 2);
        	distance = Math.sqrt(distance);
        	
//    		System.out.println("distance: " + distance);
        	
           	if (distance <= Double.parseDouble(rad)){
//        		System.out.println("under: ");
        		rtn_data.add(calc);
        	}
        	else {
//        		System.out.println("over: ");

        	}
        }
        
        /*
        for(Data d : rtn_data ) {
        	log.info("data in dataDao: " + d.toString());
        }
        */
        return rtn_data;

	}

	//queryを指定して、円形検索を行いデータ数を取得する without params
	@Override
	public int getDataCountByQueryCircleWithoutParams( String query , String ln1, String la1, String rad ) {
		
		String sql = SQL_OUTLINE + tableName + SQL_ENABLED + query;
   		log.info("getCountDataByQueryCircleWithoutParams: " + sql );

        RowMapper<Data> mapper = new BeanPropertyRowMapper<Data>(Data.class);
        List<Data> tmp_data = jdbcTemplate.query(sql, mapper);

//        int count = tmp_data.size();

//        return count;

        int count = 0;
        for  (Data calc : tmp_data){
//        	System.out.println("rec_id: " + calc.getRec_id());
        	//地点1の緯度経度（データベース上のデータ）
        	double lat1 = calc.getP_latitude();
        	double lng1 = calc.getP_longitude();
        	//地点2の緯度経度（検索中心）
        	double lat2 = Double.parseDouble( la1 );
        	double lng2 = Double.parseDouble(ln1);

        	//平均緯度
        	double latAvg = ((lat1 + ( (lat2 - lat1)/2) ) /180) * Math.PI;
        	//緯度差
        	double latDifference = (( lat1 - lat2 ) / 180 ) * Math.PI;
        	//経度差
        	double lonDifference = (( lng1 - lng2 ) / 180) * Math.PI;

        	double curRadiusTemp = 1 - 0.00669438 * Math.pow(Math.sin(latAvg), 2);
        	
        	//子午線曲率半径
        	double meridianCurvatureRadius = 6335439.327 / Math.sqrt(Math.pow(curRadiusTemp, 3));

        	//卯酉線曲率半径
        	double primeVerticalCircleCurbatureRaduis = 6378137 / Math.sqrt(curRadiusTemp);

        	//2点間の距離
        	double distance = Math.pow(meridianCurvatureRadius * latDifference, 2) +
        			Math.pow(primeVerticalCircleCurbatureRaduis * Math.cos(latAvg) * lonDifference , 2);
        	distance = Math.sqrt(distance);
//    		System.out.println("distance: " + distance);
        	if (distance <= Double.parseDouble(rad)){
//        		System.out.println("under: ");
        		count++;
        	}
        	else {
//        		System.out.println("over: ");
        	}
        }
        return count;


	}

	//queryを指定してデータのカウントを取得する with params 
	@Override
	public int getDataCountByQueryWithParams( String query , Object[] values ) {
		return 1;
	}

	//queryを指定してデータのカウントを取得する without params
	@Override
	public int getDataCountByQueryWithoutParams( String query ) {
		String sql = SQL_COUNT + tableName + SQL_ENABLED + query;
		log.info("DataCount: " + sql);
		
		int count = 0;
		count = jdbcTemplate.queryForObject(sql, Integer.class);
		
		return count;
	}

	//queryを指定してデータを取得する with params
	@Override
	public List<Data> getDataListByQueryWithParams( String query , Object[] values ){
		
   		String sql = SQL_OUTLINE + tableName + SQL_ENABLED + query;
   		log.info("getDataListByQuery: " + sql );

   		Object[] params = (Object[]) values;
//		System.out.println("params length: " + params.length );
        List<Data> data = new ArrayList<Data>();
		if (params.length > 0 ){
//			System.out.println("params[0]: " + params[0] );
	        RowMapper<Data> mapper = new BeanPropertyRowMapper<Data>(Data.class);
	        data = jdbcTemplate.query( sql , params , mapper);
		} else {
	        RowMapper<Data> mapper = new BeanPropertyRowMapper<Data>(Data.class);
	        data = jdbcTemplate.query( sql , mapper);
		}
//		System.out.println("data size: " + data.size() );

        return data;
	}

	//queryを指定してデータを取得する without params
	@Override
	public List<Data> getDataListByQueryWithoutParams( String query ) {
		
   		String sql = SQL_OUTLINE + tableName + SQL_ENABLED + query;
   		log.info("getDataListByQuery: " + sql );

        RowMapper<Data> mapper = new BeanPropertyRowMapper<Data>(Data.class);
        List<Data> data = jdbcTemplate.query( sql , mapper);
//		System.out.println("data size: " + data.size() );

        for(Data d : data ){
        	log.debug("data : " + d.toString() );
        }
        return data;
	}

	@Override
	//queryを指定して、緯度と経度を取得する with params
	public List<LocationData> getLocationDataByQueryWithParams( String query , Object[] values ) {
		
		String sql = SQL_LOCATION + tableName + SQL_ENABLED + query;
   		log.info("getLocationDataByQueryWithParams: " + sql );
		
   		Object[] params = (Object[]) values;
//		System.out.println("params length: " + params.length );
   		List<LocationData> locationData = new ArrayList<LocationData>();
		if (params.length > 0 ){
//			System.out.println("params[0]: " + params[0] );
	        RowMapper<LocationData> mapper = new BeanPropertyRowMapper<LocationData>(LocationData.class);
	        locationData = jdbcTemplate.query( sql , params , mapper);
		} else {
	        RowMapper<LocationData> mapper = new BeanPropertyRowMapper<LocationData>(LocationData.class);
	        locationData = jdbcTemplate.query( sql , mapper);
		}

		return locationData;

	}

	//queryを指定して、緯度と経度を取得する without params
	@Override
	public List<LocationData> getLocationDataByQueryWithoutParams( String query ) {


		String sql = SQL_LOCATION + tableName + SQL_ENABLED + query;
   		log.info("getLocationDataByQueryWithoutParams: " + sql );
   		List<LocationData> locationData = new ArrayList<LocationData>();
		
        RowMapper<LocationData> mapper = new BeanPropertyRowMapper<LocationData>(LocationData.class);
        locationData = jdbcTemplate.query( sql , mapper);

		return locationData;

	}

	//queryを指定して、データを取得する
	public Data getDataByQuery( String query , Object[] values) {
		
   		String sql = SQL_DETAIL + tableName + SQL_ENABLED + query ;
   		log.info("get data by query: " + sql );

        List<Data> data = new ArrayList<Data>();
		if (values.length > 0 ){
//			System.out.println("params[0]: " + params[0] );
	        RowMapper<Data> mapper = new BeanPropertyRowMapper<Data>(Data.class);
	        data = jdbcTemplate.query( sql , values , mapper);
		} else {
	        RowMapper<Data> mapper = new BeanPropertyRowMapper<Data>(Data.class);
	        data = jdbcTemplate.query( sql , mapper);
		}

		log.debug("data: " + data.get(0).toString());

		return data.get(0);

	}

}
