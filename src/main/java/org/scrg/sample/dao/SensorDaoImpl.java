package org.scrg.sample.dao;

import java.util.List;

import javax.sql.DataSource;

import org.scrg.sample.domain.Sensor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class SensorDaoImpl implements SensorDao {
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory
			.getLogger(SensorDaoImpl.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	DataSource dataSource;
	
	static private String tableName = "sensors";//テーブル名

	//すべてのSensorを取得する
	public List<Sensor> findAll() {
		
		String sql = "SELECT sensor_id, sensor_name , sensor_type , sensor_op_status , "
				+ " sensor_dt_st_type "
				+ "from " + tableName ;

		log.info("findAll: " + sql);

		RowMapper<Sensor> mapper = new BeanPropertyRowMapper<Sensor>(Sensor.class);
		List<Sensor> sensorList = jdbcTemplate.query(sql, mapper);

		// Select向けに先頭に空のデータを追加する
		sensorList.add(0, new Sensor(0, " ", 0, " ", 0, " ", 0, " "));

		return sensorList;
		
	}

	//センサIDからデータを取得する
	public Sensor findById(int id) {
		
		String sql = "SELECT sensor_id, sensor_name , sensor_type , sensor_op_status , "
				+ " sensor_dt_st_type "
				+ "from " + tableName + " where sensor_id = " + id;

		log.info("getSensorById: " + sql);

//			System.out.println("sql: " + sql);

//			Object[] params = new Object[] {limit, offSet};

		RowMapper<Sensor> mapper = new BeanPropertyRowMapper<Sensor>(Sensor.class);
		List<Sensor> sensorList = jdbcTemplate.query(sql, mapper);

		/*
    	for(Sensor sns : sensorList) {
        	System.out.println("sns_id_name: " + sns.getSns_id_name());
        	System.out.println("sns_tp: " + sns.getSns_tp());
        	System.out.println("sns_op_st: " + sns.getSns_op_st());
    	}
    	*/

		return sensorList.get(0);
	}

}
