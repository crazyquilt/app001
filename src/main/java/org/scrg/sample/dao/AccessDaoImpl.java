package org.scrg.sample.dao;

import java.util.List;

import javax.sql.DataSource;

import org.scrg.sample.domain.Access;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class AccessDaoImpl implements AccessDao {
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory
			.getLogger(AccessDaoImpl.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	DataSource dataSource;
	
	static private String tableName = "access";//テーブル名

	//すべてのaccessを取得する
	public List<Access> findAll() {
		
		String sql = "SELECT access_hst_id, access_hst_name , access_aut_type "
				+ "from " + tableName ;

		log.info("findAll: " + sql);

		RowMapper<Access> mapper = new BeanPropertyRowMapper<Access>(Access.class);
		List<Access> accessList = jdbcTemplate.query(sql, mapper);

		// Select向けに先頭に空のデータを追加する
		accessList.add(0, new Access(0," ",0, " "));

		return accessList;

	}

	//アクセス履歴IDからデータを取得する
	public Access findById(int id) {
		
		String sql = "SELECT access_hst_id, access_hst_name , access_aut_type "
				+ "from " + tableName + " where access_hst_id = " + id;

		log.info("findById: " + sql);

		RowMapper<Access> mapper = new BeanPropertyRowMapper<Access>(Access.class);
		List<Access> accessList = jdbcTemplate.query(sql, mapper);

		/*
    	for(Access acc : accessList) {
        	System.out.println("name: " + acc.getAccess_hst_name());
        	System.out.println("aut: " + acc.getAccess_hst_id());
    	}
    	*/
		return accessList.get(0);

	}

}
