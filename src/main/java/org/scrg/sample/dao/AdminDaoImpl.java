package org.scrg.sample.dao;

import java.util.List;

import javax.sql.DataSource;

import org.scrg.sample.domain.Admin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class AdminDaoImpl implements AdminDao {
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory
			.getLogger(AdminDaoImpl.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	DataSource dataSource;
	
	static private String tableName = "admins";//テーブル名

	//すべてのAdminを取得する
	public List<Admin> findAll() {

		String sql = "SELECT admin_id, admin_name from " + tableName ;

		log.info("findAll: " + sql);

		RowMapper<Admin> mapper = new BeanPropertyRowMapper<Admin>(Admin.class);
		List<Admin> adminList = jdbcTemplate.query(sql, mapper);

		// Select向けに先頭に空のデータを追加する
		adminList.add(0, new Admin(0," "));

		return adminList;
	}

	//管理者IDからデータを取得する
	public Admin findById(int id) {
		
		String sql = "SELECT admin_id, admin_name from " + tableName + " where admin_id = " + Integer.toString(id) ;

		log.info("findById: " + sql);

//			System.out.println("sql: " + sql);

//			Object[] params = new Object[] {limit, offSet};

		RowMapper<Admin> mapper = new BeanPropertyRowMapper<Admin>(Admin.class);
		List<Admin> adminList = jdbcTemplate.query(sql, mapper);

		/*
    	for(Admin adm : adminList) {
        	System.out.println("name: " + adm.getAdmin_name());
    	}
    	*/

		return adminList.get(0);

	}

}
