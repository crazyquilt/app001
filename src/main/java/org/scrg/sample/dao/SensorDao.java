package org.scrg.sample.dao;

import java.util.List;

import org.scrg.sample.domain.Sensor;

public interface SensorDao {

	//すべてのSensorを取得する
	public List<Sensor> findAll();

	//センサIDからデータを取得する
	public Sensor findById(int id);

}
