package org.scrg.sample.dao;

import java.util.List;

import org.scrg.sample.domain.Access;

public interface AccessDao {

	//すべてのaccessを取得する
	public List<Access> findAll();

	//アクセス履歴IDからデータを取得する
	public Access findById(int id);

}
