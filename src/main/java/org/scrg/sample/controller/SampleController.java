package org.scrg.sample.controller;

import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.scrg.sample.domain.Access;
import org.scrg.sample.domain.Admin;
import org.scrg.sample.domain.Data;
import org.scrg.sample.domain.LocationData;
import org.scrg.sample.domain.SearchCriteria;
import org.scrg.sample.domain.Sensor;
import org.scrg.sample.domain.ShowData;
import org.scrg.sample.domain.User;
import org.scrg.sample.service.AccessService;
import org.scrg.sample.service.AdminService;
import org.scrg.sample.service.DataService;
import org.scrg.sample.service.SensorService;
import org.scrg.sample.util.Manipulator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.site.SitePreference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.drew.imaging.ImageProcessingException;

@Controller
public class SampleController {
	
	@Autowired
	ServletContext context;
	
	@Autowired
	ServletConfig config;
	
	@Autowired
	DataService dataService;
	
	@Autowired
	SearchCriteria searchCriteria;
	
	@Resource
	private String[] searchTypes;

	@Resource
	private String[] contentsTypes;
	
	@Resource
	private String tempPath;
	
	@Resource
	private String defaultDate;
	
	@Resource
	private String defaultLatitude;
	
	@Resource
	private String defaultLongitude;
	
	@Resource
	private String degreePerSecond;
	
	@Resource
	private String meterPerSecondLat;
	
	@Resource
	private String meterPerSecondLng;
	
	@Resource
	private String distancePerDegreeLng;
	
	@Resource
	private String distancePerDegreeLat;
	
	@Resource
	private String linePerPage;
	
	@Resource
	private String linePerPageREST;

	@Autowired
	private AccessService accessService;

	@Autowired
	private AdminService adminService;

	@Autowired
	private SensorService sensorService;
	
	@Autowired
	private String storagePathRawdata;
	
	@Autowired
	private String storagePathResult;
	
	@ModelAttribute("allAccess")
	public List<Access> populateAccess() {
		return this.accessService.findAll();
	}
	
	@ModelAttribute("allAdmin")
	public List<Admin> populateAdmins() {
		return this.adminService.findAll();
	}
	
	@ModelAttribute("allSensor")
	public List<Sensor> populateSensors() {
		return this.sensorService.findAll();
	}
	
	static int pageMax = 0;
	static int pageNo = 0;
	//------------------------------
	//検索結果の位置情報を全件保持する
	static List<LocationData> locationData;

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory
			.getLogger(SampleController.class);
	
	@RequestMapping("/" )
	public String home( Principal principal , Model model , Device device ,
			SitePreference sitePreference ) {
		log.info("home:");
		log.trace("device:" + device.toString() + " / preference : " + sitePreference.name());
		
//		System.out.println("context path: " + context.getContextPath());

        if (device.isMobile()) {
            log.info("Hello mobile user!");      
        } else if (device.isTablet()) {
            log.info("Hello tablet user!");         
        } else {
            log.info("Hello desktop user!");         
        }
        
		model.addAttribute("showData", new ShowData());

		model.addAttribute("user", new User());
		
		model.addAttribute("searchTypes", searchTypes);
		model.addAttribute("contentsTypes", contentsTypes);
		model.addAttribute("searchCriteria", searchCriteria);

		log.trace("searchCriteria: " + searchCriteria.toString());

		/*
		for(String s : searchTypes) {
			log.info("search: " + s);
		}
		for(String c : contentsTypes) {
			log.info("content: " + c);
		}
		*/

		return ("home");
	}
	
	@RequestMapping("FileUpload")
	public String fileUpload( Principal principal , Model model , Device device ,
			SitePreference sitePreference ) {
		log.info("fileUpload:");
		log.trace("device:" + device.toString() + " / preference : " + sitePreference.name());
		
//		System.out.println("context path: " + context.getContextPath());

        if (device.isMobile()) {
            log.info("Hello mobile user!");      
        } else if (device.isTablet()) {
            log.info("Hello tablet user!");         
        } else {
            log.info("Hello desktop user!");         
        }
        
		model.addAttribute("showData", new ShowData());

		model.addAttribute("user", new User());
		
		model.addAttribute("searchTypes", searchTypes);
		model.addAttribute("contentsTypes", contentsTypes);
		model.addAttribute("searchCriteria", searchCriteria);

		log.trace("searchCriteria: " + searchCriteria.toString());

		/*
		for(String s : searchTypes) {
			log.info("search: " + s);
		}
		for(String c : contentsTypes) {
			log.info("content: " + c);
		}
		*/

		return ("upload");
	}
	
	@RequestMapping(value="/ajax/test" , method=RequestMethod.POST,
			headers="Accept=application/xml, application/json")
	public @ResponseBody String ajaxReceive(@RequestBody User user){
		log.info("ajax receive:");
		String msg = "<b>" + user.getName() + " is " + user.getAge() + " years old.</b>";
//		return "home";
		return msg;
	}
	
	@RequestMapping(value="/ajax/fileTest" , method=RequestMethod.POST)
	public @ResponseBody String ajaxFile( MultipartHttpServletRequest multipart ){
		log.info("ajax fileTest:");
		String msg = "<b>" + multipart.getContentType() + "</b>";
//		return "home";
		return msg;
	}
	
	
	/**
	 * ajax/fileScan
	 * 
	 * @param multipart
	 * @param response
	 * @param principal
	 * @param model
	 * @return
	 * @throws ImageProcessingException
	 * @throws ParseException
	 * @throws IOException
	 */
	@RequestMapping(value={"/ajax/fileScan","/**/ajax/fileScan"} , method=RequestMethod.POST,
			headers="Accept=application/xml, application/json")
	@ResponseStatus(value=HttpStatus.OK)
	public @ResponseBody ShowData fileScan( MultipartHttpServletRequest multipart ,
				HttpServletResponse response , Principal principal , Model model ) throws ImageProcessingException, ParseException, IOException {
		
		log.info("ajax/fileScan:");
		
		Manipulator manipulator = new Manipulator();
		ShowData showData = new ShowData();
		showData = manipulator.getExif(multipart);

		Access access = new Access();
		int acc_hst_id = manipulator.accessHistoryIdGet();
		access = accessService.findById(acc_hst_id);
		showData.setAcc_hst_id(Integer.toString(access.getAccess_hst_id()));
		showData.setAcc_aut_tp(Integer.toString(access.getAccess_aut_type()));
		
		Admin admin = new Admin();
		int adm_id = manipulator.adminIdGet();
		admin = adminService.findById(adm_id);
		showData.setAdm_id(Integer.toString(admin.getAdmin_id()));

		Sensor sensor = new Sensor();
		int sensor_id = manipulator.sensorIdGet();
		sensor = this.sensorService.findById(sensor_id);
		showData.setSns_id(Integer.toString(sensor.getSensor_id()));
		showData.setSns_op_st(Integer.toString(sensor.getSensor_op_status()));
		showData.setSns_tp(Integer.toString(sensor.getSensor_type()));
		showData.setSns_dt_st_tp(Integer.toString(sensor.getSensor_dt_st_type()));

//		System.out.println("showData ocr_time: " + showData.getOcr_time());
		
		model.addAttribute("showData", showData);

		return showData;
	}

	@RequestMapping(value="/ajax/fileAdd" , method=RequestMethod.POST)
	@ResponseStatus(value=HttpStatus.OK)
	public @ResponseBody String ajaxFileAdd( MultipartHttpServletRequest multipart, 
			HttpServletResponse response , Principal principal) {
		
		if (principal !=null){
			String loggedUserName = principal.getName();
			log.info("ajax file upload to storage: " + loggedUserName);
		}
		log.info("storage path in ajax/fileAdd: " + storagePathRawdata);
//		log.info("multipart: " + multipart);

		Iterator<String> itr =  multipart.getFileNames();
//		log.info("itr.toString: " + itr.toString());
//		log.info("itr.getClass: " + itr.getClass());
//		log.info("itr.hasNext: " + itr.hasNext());
		MultipartFile mpf = multipart.getFile(itr.next());
//		log.info("mpf.getOriginalFilename: " + mpf.getOriginalFilename());

		Manipulator manipulator = new Manipulator();
		String rawFileName = manipulator.getStoreFile(mpf.getOriginalFilename(), multipart , context, principal , 
				storagePathRawdata , storagePathResult);
//		log.info("rawFileName: " + rawFileName);
//	    System.out.println("file: " + rawFileName);
		String rawFileNameFullPath = storagePathRawdata + File.separator + rawFileName;
		log.info("store to: " + rawFileNameFullPath);
//	    System.out.println("store to: " + rawFileNameFullPath);

	    //受け取ったstreamをStorage用の場所に書き込むためmpfをサービスに渡す
	    try {
			dataService.rawFileWrite(mpf.getInputStream(), rawFileNameFullPath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
//		return "ok";
//		return rawFileName;
//	    System.out.println("ajax/file/upload store to: " + rawFileName);
		return rawFileName;

	}

	@RequestMapping(value="/ajax/dataAdd", method=RequestMethod.POST,
			headers="Accept=application/xml, application/json")
	@ResponseStatus(value=HttpStatus.OK)
	public @ResponseBody String postDataAjax(@RequestBody ShowData jsonData , Principal principal) {

		if (principal !=null){
			String loggedUserName = principal.getName();
			log.info("ajax dataAdd: " + loggedUserName);
		}
		log.info("dataAdd: " );

		Manipulator manipulator = new Manipulator();
	    Data data = new Data();
	    data = manipulator.convertToData(jsonData);
	    
//	    String msg = "recId: " + recId + " is successfully updated";
//	    String msg = "successfully updated";
	    String msg = dataService.addData(data);
//	    System.out.println("msg: " + msg);
		log.info("msg: " + msg );
	    
		return "/.";

	}
	
	@RequestMapping("/SearchPage")
	public String searchForm(Model model) {

		log.info("searchPage: " );

//		ShowData showData = new ShowData();
//		model.addAttribute("showData", showData);
		model.addAttribute("searchTypes", searchTypes);
		model.addAttribute("contentsTypes", contentsTypes);
		model.addAttribute("searchCriteria", searchCriteria);

		log.info("searchCriteria: " + searchCriteria.toString());

		/*
		for(String s : searchTypes) {
			log.info("search: " + s);
		}
		
		*/
		return "search";
	}
	
	@RequestMapping("/search")
	public String search(@ModelAttribute SearchCriteria searchCriteria , Model model) {

		log.info("search: " + searchCriteria.toString());

		return "search";
	}

	@RequestMapping(value="/mobile/search" , method=RequestMethod.POST)
	public @ResponseBody List<Data> mobileSearch(@RequestBody SearchCriteria searchCriteria , Model model) {

		log.info("mobileSearch: " + searchCriteria.toString());

		String la1 = searchCriteria.getLatitude1();
		String ln1 = searchCriteria.getLongitude1();
		String rad = searchCriteria.getRadius();
		int count = 0;
		
		String ret = querySetCircle( la1 , ln1 , rad );
		
		count = dataService.getDataCountByQueryCircleWithoutParams( ret , ln1 , la1 , rad );

		log.info("count= " + count);
		
		List<Data> listData = dataService.getDataListByQueryCircleWithoutParams(ret, ln1, la1, rad);

		model.addAttribute("data", listData);
		for ( Data d : listData ) {
			log.info("data: " + d.toString());
		}
		
		return listData;
	}

	public String querySetCircle( String la1 , String ln1 , String rad ) {
		log.info("querySetCircle: " + la1 + ":" + ln1 + ":" + rad );
		String query;
		
		//1秒あたりの角度
		String dSecond = degreePerSecond;
		log.info("init param degreePerSecond: " + dSecond);

		//1秒当たりの距離（メートル）
		String mSecondLat = meterPerSecondLat;
		String mSecondLng = meterPerSecondLng;
		log.info("init param meterPerSecondLat: " + mSecondLat);
		log.info("init param meterPerSecondLng: " + mSecondLng);
//		System.out.println("init param meterPerSecond: " + mSecond);

		double d_distance = Double.parseDouble(rad);
		double diffLat = d_distance /  Double.parseDouble(mSecondLat) * Double.parseDouble(dSecond) ;
		double diffLng = d_distance /  Double.parseDouble(mSecondLng) * Double.parseDouble(dSecond) ;
//		System.out.println("diff: " + diff);


		double longitude1 = Double.parseDouble(ln1) - diffLng ;
		double longitude2 = Double.parseDouble(ln1) + diffLng ;
		log.info("longitude1: " + longitude1);
		log.info("longitude2: " + longitude2);


		double latitude1 = Double.parseDouble(la1) - diffLat;
		double latitude2 = Double.parseDouble(la1) + diffLat;
		log.info("latitude1: " + latitude1);
		log.info("latitude2: " + latitude2);

		String queryStr = " AND MBRContains(GeomFromText(\'LineString("
				+ longitude1 + " " + latitude1 + ", " + longitude2 + " " + latitude2  
				+ " )\'), LOCATION) " ; 

		log.info("query for circle search: " + queryStr);
		
		return queryStr;
	}
	
	@RequestMapping("Registration")
	public String registration(Principal principal , Model model , Device device ,
			SitePreference sitePreference) {
		
		log.info("registration:");
		log.trace("device:" + device.toString() + " / preference : " + sitePreference.name());
		
//		System.out.println("context path: " + context.getContextPath());

        if (device.isMobile()) {
            log.info("Hello mobile user!");      
        } else if (device.isTablet()) {
            log.info("Hello tablet user!");         
        } else {
            log.info("Hello desktop user!");         
        }
        
		model.addAttribute("showData", new ShowData());

		model.addAttribute("user", new User());
		
		model.addAttribute("searchTypes", searchTypes);
		model.addAttribute("contentsTypes", contentsTypes);
		model.addAttribute("searchCriteria", searchCriteria);

		log.trace("searchCriteria: " + searchCriteria.toString());

		/*
		for(String s : searchTypes) {
			log.info("search: " + s);
		}
		for(String c : contentsTypes) {
			log.info("content: " + c);
		}
		*/

		return "home";
	}
	
	@RequestMapping("DataList")
	public String dataList(Model model) {
		
		log.info("DataList:");
		
		String queryStr="";
		int count = dataService.getDataCountByQueryWithoutParams(queryStr);
		log.debug("count: " + count);
		List<Data> dataList = dataService.getDataListByQueryWithoutParams(queryStr);
		model.addAttribute("dataList", dataList);
		
		pageMax = ( ( count - 1 ) / Integer.parseInt(linePerPage )) + 1;
		log.debug("pageMax: " + pageMax);
		pageNo = 1;
		log.debug("pageNo: " + pageNo);
    	int offSet = Integer.parseInt(linePerPage) * ( pageNo - 1 );
		log.debug("offSet: " + offSet);
		int limit = Integer.parseInt(linePerPage);
		log.debug("limit: " + limit);
		
		locationData = dataService.getLocationDataByQueryWithoutParams(queryStr);
		queryStr = " limit ? , ? ";
		Object[] values = { offSet , limit };
		
    	List<Data> data = dataService.getDataListByQueryWithParams(queryStr, values);
//    	List<ShowData> showData = manipulator.convertList(data,request);

		model.addAttribute( "locationDataList" , locationData );
		model.addAttribute("data", data);

		model.addAttribute("pageNo", pageNo);
		model.addAttribute("pageMax", pageMax);
		
		return "list" ;
	}
	
	@RequestMapping("/detail")
	public String dataDetail(@RequestParam("id") String id , Model model , HttpServletRequest request) {
		
		log.debug("id: " + id);
		
		String queryStr = "";
		Object[] values = { id };
		queryStr = " AND rec_id = ? ";

		Data data = dataService.getDataByQuery(queryStr, values);
		log.debug("data: " + data.toString());
		Manipulator manipulator = new Manipulator();
		ShowData showData = manipulator.convertToShowMobile(data, request);
		model.addAttribute("data", showData);
		return "detail";
	}
}
