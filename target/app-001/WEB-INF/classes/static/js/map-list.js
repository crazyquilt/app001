var map;
var la1;
var ln1;
var la2;
var ln2;

$(document).ready(function() {

	var result_icon = new google.maps.MarkerImage(
		    'http://maps.google.co.jp/mapfiles/ms/icons/yellow-dot.png',
		    new google.maps.Size(32,32),  // size
		    new google.maps.Point(0,0),   // origin
		    new google.maps.Point(16,32)  // anchor
		  );

	var result_icon2 = new google.maps.MarkerImage(
		    'http://maps.google.co.jp/mapfiles/ms/icons/ltblue-dot.png',
		    new google.maps.Size(32,32),  // size
		    new google.maps.Point(0,0),   // origin
		    new google.maps.Point(16,32)  // anchor
		  );

	map = new GMaps({
		div: '#map_list',
		zoom: 13,
		lat: 36.06209214288428,
		lng:140.1357865333557
	});

    var result_tbl2 = document.getElementById("locationtable");
	var rows2 = result_tbl2.rows.length;
//	alert("rows count: " + rows2);
	var cols2 = result_tbl2.rows[0].cells.length;
//	alert("cols count: " + cols2);
	
	for (var i =1 ; i<rows2 ; i++) {
		ln1 = result_tbl2.rows[i].cells[2].innerHTML;
		la1 = result_tbl2.rows[i].cells[1].innerHTML;
//		alert("la: " + la1 + " ln: " + ln1);

		map.addMarker({
			lat:la1,
			lng:ln1,
			icon: result_icon2,
			title : result_tbl2.rows[i].cells[0].innerHTML
		});

	}
//	$("#locationbox").css("display" , "none");

    var result_tbl = document.getElementById("listtable");
	var rows = result_tbl.rows.length;
//	alert("rows count: " + rows);
	cols = result_tbl.rows[0].cells.length;
//	alert("cols count: " + cols);

	
	for (var i =1 ; i<rows ; i++) {
		ln1 = result_tbl.rows[i].cells[2].innerHTML;
		la1 = result_tbl.rows[i].cells[3].innerHTML;
//		alert("la: " + la1 + " ln: " + ln1);

		map.addMarker({
			lat:la1,
			lng:ln1,
			icon: result_icon,
			title : result_tbl.rows[i].cells[0].innerHTML
		});

	}

});
