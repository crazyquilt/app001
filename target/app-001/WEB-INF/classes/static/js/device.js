// ウィンドウサイズを取得するJavaScript
//

$(function() {

	var agent = navigator.userAgent;
//	alert("agent= " + agent);

    var ww = screen.width;

	if ((agent.indexOf('iPhone') > 0 && agent.indexOf('iPad') == -1) || agent.indexOf('iPod') > 0 || agent.indexOf('Android') > 0) {
//		alert("mobile or tablet ");
		width = ww;

	    $("body").width(width);
	    $("#header").width(width);
	    $("#main").width(width);
	    $("#uploadBox").width(width);
	    $("#map_search").width(width* 0.9);
	    $("#map_search").height(width*0.6);

	}

});
