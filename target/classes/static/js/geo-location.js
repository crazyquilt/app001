// 位置情報を取得するJavaScript
//

var map;
var la1;
var ln1;
var la2;
var ln2;
var lat,lng;

$(function() {

//	alert('geo-location is being loaded');

	setCsrfTokenToAjaxHeader();

	var image1 = new google.maps.MarkerImage('resources/images/n001.png');

	var result_icon = new google.maps.MarkerImage(
		    'http://maps.google.co.jp/mapfiles/ms/icons/yellow-dot.png',
		    new google.maps.Size(32,32),  // size
		    new google.maps.Point(0,0),   // origin
		    new google.maps.Point(16,32)  // anchor
		  );

	var geolocation;
	try {
		if(typeof(navigator.geolocation) == 'undefined') {
			geolocation = google.gears.factory.create('beta.geolocation');
		} else {
			geolocation = navigator.geolocation;
		}
	} catch (e) {}
	
	if (!geolocation) {
		alert('位置情報は利用できません');
	    return;
	}
	
	var watchId;
	var success = function(position) {
        $('#latitude').html(position.coords.latitude); //緯度
        $('#longitude').html(position.coords.longitude); //経度
        
    	try {
    		map = new GMaps({
    			div: '#map_search',
    			zoom: 15,
    			lat: position.coords.latitude,
    			lng: position.coords.longitude
//    			lat: 36.06209214288428,
//    			lng:140.1357865333557
    		});

    		la1 = position.coords.latitude;
    		ln1 = position.coords.longitude;

    		la2 = 36.0601382;
    		ln2 = 140.1375084;
    		
    		var ra = "10";
    		map.addMarker({
    			lat:la1,
    			lng:ln1,
    			icon: image1,
    			draggable : true,
    			title: '現在地',
    			dragend: function(e) {
//    				alert('dragend');
    				la1 = e.latLng.lat();
//    				alert('lat: ' + lat);
    				ln1 = e.latLng.lng();
//    				alert( 'lat: ' + la1 + ' lng: ' + ln1 );

    				GMaps.geocode({
    					lat: la1,
    					lng: ln1,
    					callback: function(results, status) {
    						if ( status == 'OK' ){
//    							alert('results[0]: ' + results[0]);
    							document.getElementById('CRaddress1').innerHTML = results[0].formatted_address
    							.replace(/^日本, /, '');
//    							$("#CRaddress1").innerHTML = results[0].formatted_address
//    							.replace(/^日本, /, '');
    					        $('#latitude').html(la1); //緯度
    					        $('#longitude').html(ln1); //経度
//    							document.criteriaForm.latitude1.value = la1;
//   							document.criteriaForm.longitude1.value = ln1;
    						}
    					}
    				});
    			}

    		});
    		GMaps.geocode({
    			lat: la1,
    			lng: ln1,
    			callback: function(results, status) {
    				if ( status == 'OK' ){
//    					alert('results[0]: ' + results[0]);
    					document.getElementById('CRaddress1').innerHTML = results[0].formatted_address
    					.replace(/^日本, /, '');
    			        $('#latitude').html(la1); //緯度
    			        $('#longitude').html(ln1); //経度
    				}
    			}
    		});

        	//現在の位置情報を利用してデータの検索を行う
        	var searchUrl = "mobile/search";
//    		alert('url: ' + searchUrl);
    		
    		var searchCriteria = {
    				'accessCR' : "0",
    				'adminCR' : "0",
    				'sensorCR' : "0",
    				'latitude1' : String(la1),
    				'longitude1' : String(ln1),
    				'latitude2' : "0",
    				'longitude2' : "0",
    				'radius' : ra,
    				'offSet' : "0",
    				'limit' : "0",
    				'how': "circle",
    				'contentType' : "0",
    				'byDate' : "true"
    			};
//    		alert('search Criteria: ' + JSON.stringify(searchCriteria));
    		$.ajax({
    			url : searchUrl ,
    			type : 'POST',
    			contentType: "application/json;charset=UTF-8",
//    			data : jsonData,
    			data : JSON.stringify(searchCriteria),
//    			contentType : "application/x-www-form-urlencoded; charset=UTF-8",
    			dataType : 'json',
//    			dataType : 'text',
    			success : function(data, tetStatus, xhr) {
//    				alert("receive from mobile/search: " + JSON.stringify(data) );
    				
    				// データを分解し、マーカーを作成後地図に表示する

					var circleOptions = {
					        center: new google.maps.LatLng(la1, ln1),
//					        radius: 200,
					        radius: parseInt(ra),
					        strokeColor: "#55555",
					        strokeOpacity: 0.5,
					        strokeWeight: 0.3,
					        fillColor: "#555555",
					        fillOpacity: 0.35
					};

					map.drawCircle( circleOptions );

    				var j = data;
					var htmlStr = "";
    				for (var i=0 ; i<j.length; i++) {
//    					alert(i + " : latitude : " + j[i].p_latitude + " / longitude : " + j[i].p_longitude );
    				/*
    				$.each(data, function( i , value ) {
    					alert(i + " : " + JSON.storingify(value) );
    				});
    				*/
//    					alert(i + " : " + j[i].rec_id + " : " + j[i].sns_rw_dt_name );
    			        $('#results').html(j[i].rec_id + ": " + j[i].sns_rw_dt_name ); //rec_id
    			        htmlStr += "<a href =\"detail?id=" + j[i].rec_id + "\">" + j[i].rec_id +"</a> <br/>";
//    					alert( "htmlStr : " + htmlStr );

    					map.addMarker({
    						lat:j[i].p_latitude,
    						lng:j[i].p_longitude,
    						icon: result_icon,
//    						draggable : true,
    						title: String(i+1),
						});
    				}
			        $('#results').html(htmlStr ); 

    			},
    			error : function(XMLHttpRequest,
    					textStatus, errorThrown) {
    				alert("mobile/search ajax通信エラー" + XMLHttpRequest.status + " : " +
    						textStatus + " : " + errorThrown );
    			}
    		});

    	} catch(e) {}

	}
	
	var error = function (error) {
		
	}
	
	watchId = geolocation.getCurrentPosition(success,error);
	var image1 = new google.maps.MarkerImage('resources/images/n001.png');
	var image2 = new google.maps.MarkerImage('resources/images/n002.png');

});