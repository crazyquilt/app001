// Home
// contentType別に色を変えたマーカーを表示する
// マーカーをクリックすることで対応する画像を表示
//

var map;
var la1;
var ln1;
var la2;
var ln2;

$(document).ready(function() {
//	alert("map-total: " );
	
	var type1_icon = new google.maps.MarkerImage('resources/images/imageicon1.png');
	var type2_icon = new google.maps.MarkerImage('resources/images/imageicon2.png');
	var type3_icon = new google.maps.MarkerImage('resources/images/imageicon3.png');
	var type4_icon = new google.maps.MarkerImage('resources/images/imageicon4.png');
	var type5_icon = new google.maps.MarkerImage('resources/images/imageicon5.png');

	try {
	map = new GMaps({
		div: '#map_total',
		zoom: 13,
		styles: [{
		       stylers: [
		          {   hue: "#2985f5"},
		          {   gamma: 1.50},
		          {   saturation: -40}]
		        }],
		lat: 36.06209214288428,
		lng:140.1357865333557
	});
	
	var rows = totaltable.rows.length;

	for (var i =1 ; i<rows ; i++) {
		ln1 = totaltable.rows[i].cells[3].innerHTML;
		la1 = totaltable.rows[i].cells[2].innerHTML;
		
//		var url = urltable.rows[i].cells[0].innerHTML + ">"; 
		var url = urltable.rows[i].cells[0].innerHTML; 
//		alert("url: " + url );

		var title = totaltable.rows[i].cells[0].innerHTML; 
//		alert("before url: " + url );
		
		var icon_type;
		if (totaltable.rows[i].cells[1].innerHTML.indexOf("image") == 0){
			icon_type=type1_icon;
			url = "<image src=\"" + url + "\" width=\"200\" >";
//			alert("url: " + url );
			
			map.addMarker({
				lat:la1,
				lng:ln1,
				icon: icon_type,
				infoWindow: {
					content: url
				},
				title: title
			});

		} 
		else if (totaltable.rows[i].cells[1].innerHTML.indexOf("audio") == 0){
			icon_type=type2_icon;
//			url = "<audio src=\"" + url + "\"><p>音声を再生するにはaudioタグをサポートしたブラウザが必要です。</p></audio>";
//			alert("url: " + url );
			map.addMarker({
				lat:la1,
				lng:ln1,
				icon: icon_type,
				title: title
			});
		}
		else if (totaltable.rows[i].cells[1].innerHTML.indexOf("video") == 0 ){
			icon_type=type3_icon;
//			url = "<video src=\"" + url + "\" ><p>動画を再生するにはvideoタグをサポートしたブラウザが必要です。</p></video>";
			map.addMarker({
				lat:la1,
				lng:ln1,
				icon: icon_type,
				title: title
			});
		}
		else if (totaltable.rows[i].cells[1].innerHTML.indexOf("application") == 0 ){
			icon_type=type4_icon;
			map.addMarker({
				lat:la1,
				lng:ln1,
				icon: icon_type,
				title: title
			});
		}
		else {
			icon_type=type5_icon;
			map.addMarker({
				lat:la1,
				lng:ln1,
				icon: icon_type,
				title: title
			});
		}
		/*
		map.addMarker({
			lat:la1,
			lng:ln1,
			icon: icon_type,
			infoWindow: {
				content: url
			},
			title: title
		});*/
	}

	$("#contentbox").css("display" , "none");
	$("#urlbox").css("display" , "none");
	} catch(e) {
//		alert(e);
	}

});
