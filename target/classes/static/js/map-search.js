// Java Script for searching

var map;
var la1;
var ln1;
var la2;
var ln2;

$(document).ready(function() {
//	alert("map-search: " );

	var image1 = new google.maps.MarkerImage('resources/images/n001.png');

	var image2 = new google.maps.MarkerImage('resources/images/n002.png');

	var result_icon = new google.maps.MarkerImage(
		    'http://maps.google.co.jp/mapfiles/ms/icons/yellow-dot.png',
		    new google.maps.Size(32,32),  // size
		    new google.maps.Point(0,0),   // origin
		    new google.maps.Point(16,32)  // anchor
		  );

	try {
	map = new GMaps({
		div: '#map_search',
		zoom: 13,
		lat: 36.06209214288428,
		lng:140.1357865333557
	});

	la1 = 36.0640921;
	ln1 = 140.1330258;

	la2 = 36.0601382;
	ln2 = 140.1375084;

	map.addMarker({
		lat:la1,
		lng:ln1,
		icon: image1,
		draggable : true,
		title: '地点1',
		dragend: function(e) {
//			alert('dragend');
			la1 = e.latLng.lat();
//			alert('lat: ' + lat);
			ln1 = e.latLng.lng();
//			alert( 'lat: ' + la1 + ' lng: ' + ln1 );

			GMaps.geocode({
				lat: la1,
				lng: ln1,
				callback: function(results, status) {
					if ( status == 'OK' ){
//						alert('results[0]: ' + results[0]);
						document.getElementById('CRaddress1').innerHTML = results[0].formatted_address
						.replace(/^日本, /, '');
//						$("#CRaddress1").innerHTML = results[0].formatted_address
//						.replace(/^日本, /, '');
						document.criteriaForm.latitude1.value = la1;
						document.criteriaForm.longitude1.value = ln1;
					}
				}
			});
		}

	});

	map.addMarker({
		lat:la2,
		lng:ln2,
		icon: image2,
		draggable : true,
		title: '地点2',
		dragend: function(e) {
//			alert('dragend');
			la2 = e.latLng.lat();
//			alert('lat: ' + lat);
			ln2 = e.latLng.lng();
//			alert( 'lat: ' + la1 + ' lng: ' + ln1 );

			GMaps.geocode({
				lat: la2,
				lng: ln2,
				callback: function(results, status) {
					if ( status == 'OK' ){
//						alert('results[0]: ' + results[0]);
						document.getElementById('CRaddress2').innerHTML = results[0].formatted_address
						.replace(/^日本, /, '');
//						$("#CRaddress1").innerHTML = results[0].formatted_address
//						.replace(/^日本, /, '');
						document.criteriaForm.latitude2.value = la2;
						document.criteriaForm.longitude2.value = ln2;
					}
				}
			});

		}
	});

	GMaps.geocode({
		lat: la1,
		lng: ln1,
		callback: function(results, status) {
			if ( status == 'OK' ){
//				alert('results[0]: ' + results[0]);
				document.getElementById('CRaddress1').innerHTML = results[0].formatted_address
				.replace(/^日本, /, '');
			}
		}
	});

	document.criteriaForm.latitude1.value = la1;
	document.criteriaForm.longitude1.value = ln1;

	GMaps.geocode({
		lat: la2,
		lng: ln2,
		callback: function(results, status) {
			if ( status == 'OK' ){
//				alert('results[0]: ' + results[0]);
				document.getElementById('CRaddress2').innerHTML = results[0].formatted_address
				.replace(/^日本, /, '');
			}
		}
	});

	document.criteriaForm.latitude2.value = la2;
	document.criteriaForm.longitude2.value = ln2;
	} catch(e) {
//		alert(e);
	}

});
